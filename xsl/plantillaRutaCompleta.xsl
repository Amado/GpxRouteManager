<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>
<xsl:template match="/gpx/trk"><!--indicamos el nodo que queremos recorrer -->
<html>

<body>
<li>
	Ruta :<br/><br/>
		<xsl:text disable-output-escaping="yes"><![CDATA[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]]></xsl:text>
			Titulo : <xsl:value-of select="name"/><br/><br/>
		<xsl:text disable-output-escaping="yes"><![CDATA[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]]></xsl:text>
			Descripcion : <xsl:value-of select="desc"/><br/><br/>
	Puntos :<br/><br/>
</li>

  <xsl:for-each select="trkseg/trkpt">
	<li>
		<xsl:text disable-output-escaping="yes"><![CDATA[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]]></xsl:text>
		Punto :<br/>
		<xsl:text disable-output-escaping="yes"><![CDATA[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]]></xsl:text>
			Elevacion : <xsl:value-of select="ele"/><br/>
		<xsl:text disable-output-escaping="yes"><![CDATA[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]]></xsl:text>
			Hora : <xsl:value-of select="time"/><br/>
		<xsl:text disable-output-escaping="yes"><![CDATA[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]]></xsl:text>
			Latitud : <xsl:value-of select="@lat"/><br/>
		<xsl:text disable-output-escaping="yes"><![CDATA[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]]></xsl:text>
			Longitud : <xsl:value-of select="@lon"/><br/>
	</li><br/><br/>

</xsl:for-each> 

</body>

</html>

</xsl:template>

</xsl:stylesheet>
