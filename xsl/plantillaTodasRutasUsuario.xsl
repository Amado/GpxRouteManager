<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>
<xsl:template match="/resumenes"><!--indicamos el nodo que queremos recorrer -->
<html>

<body>
<li>
	Usuario : <xsl:value-of select="@usuario"/><br/><br/>
</li>

  <xsl:for-each select="resumen">
	<li>
		Ruta :<br/><br/>
		<xsl:text disable-output-escaping="yes"><![CDATA[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]]></xsl:text>
			Nombre de la ruta : <xsl:value-of select="ruta_nombre"/><br/><br/>
		<xsl:text disable-output-escaping="yes"><![CDATA[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]]></xsl:text>
			Descripcion : <xsl:value-of select="descripcion"/><br/><br/>
		<xsl:text disable-output-escaping="yes"><![CDATA[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]]></xsl:text>
			Distancia : <xsl:value-of select="distancia"/><br/><br/>
		<xsl:text disable-output-escaping="yes"><![CDATA[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]]></xsl:text>
			Desnivel : <xsl:value-of select="desnivel"/><br/><br/>
	</li><br/><br/>

</xsl:for-each> 

</body>

</html>

</xsl:template>

</xsl:stylesheet>
