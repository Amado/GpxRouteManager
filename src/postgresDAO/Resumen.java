package postgresDAO;


/**
 * Modela el resumen de una ruta.gpx
 * 
 * @author Emilio Amado 2015
 *
 */
public class Resumen {
	
	int id;
	String nombre;
	String descripcion; 
	double desnivel;
	double distancia;
	String nombreUsuario;
	int idUsuario;
	
	@Override
	public String toString() {
		return " \nRuta :\n\t id=" + id + "\n\t nombre=" + nombre + "\n\n\t descripcion="
				+ descripcion + "\n\n\t desnivel=" + desnivel + "\n\t distancia="
				+ distancia + "\n\t nombreUsuario=" + nombreUsuario
				+ "\n\t idUsuario=" + idUsuario + "";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public double getDesnivel() {
		return desnivel;
	}

	public void setDesnivel(double desnivel) {
		this.desnivel = desnivel;
	}

	public double getDistancia() {
		return distancia;
	}

	public void setDistancia(double distancia) {
		this.distancia = distancia;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Resumen(int id, String nombre, String descripcion, double desnivel,
			double distancia, String nombreUsuario, int idUsuario) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.desnivel = desnivel;
		this.distancia = distancia;
		this.nombreUsuario = nombreUsuario;
		this.idUsuario = idUsuario;
	}
}
