package postgresDAO;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import dbServersMain.TUI;


/**
 * Data Access Object para realizar consultas sobre la BD local de postgresQl,
 * concretamente sobre la BD 'gpxdb'.
 * 
 * <p>Si se desea acceder a la BD desde el shell de una terminal, usar:
 * 
 * 	1. Logearse como superUser en el sistema :
 * 		
 * 		$ '-su' + pass
 * 
 *	2. Acceder a la BD :
 *
 *		# 'psql -U test -d gpxdb -h 127.0.0.1 -W' + pass
 *
 *	3. Para ver una relación de las tablas :
 *
 *		gpxdb=# '\d'
 *
 *	4. Para ver las especificaciones de una tabla :
 *
 *		gpxdb=# '\d nombreTabla'
 *
 *	5. Restaurar la DB :
 *
 *		gpxdb=# \i /tmp/gpx.sql 
 * 
 * @author Emilio Amado 2015
 *
 */
public class PostgresDAO {
	
	private static Connection con;
	
	
	/**
	 * Inerta un nuevo registro de 'usuario' dentro de la tabla 'usuarios'.
	 * Sintaxis : 'insert into usuarios (nombre,pass) values ('xxxx','xxxxxx');'.
	 * 
	 * @return true si se ha tenido éxito, false en otro caso
	 */
	public static boolean insertIntoUsuarios(String user, String pass){
		
		try{
			//primero comprobamos que le nombre de usuario no esté ya cogido
			Statement st =con.createStatement();
			ResultSet rs = st.executeQuery("select * from usuarios");
			
			//almacenamos el resultado de la query en un Array
			ArrayList<String> nombres = new ArrayList<String>();
			
			if( rs != null ){
				while( rs.next() ){
					String nombre = rs.getString("nombre");
					nombres.add(nombre);
				}
				
			}
			
			//recorremos el array en busca de coincidencias
			for(String s : nombres){
				if(s.equals(user)){
					TUI.printError("el nombre ya existe");
					return false;
				}
			}

			//si el nombre de usuario no existe realizamos la inserción
			String query = "insert into usuarios (nombre,pass) values (?,?)";
			PreparedStatement pSt = con.prepareStatement(query);
			
			pSt.setString(1, user);
			pSt.setString(2, pass);
			pSt.execute();	
				
			return true;
			
		}catch(SQLException e){
			e.printStackTrace();
		}
		return false;
	}
	
	
	/**
	 * valida el login del usuario en la BD gpxdb de postgresQL.
	 * Sintaxis : 'SELECT * FROM usuarios WHERE nombre = 'xxx';'
	 * Se obtiene un ResultSet, si el valor del ResultSet es 0, 
	 * el usuario no existe, diferente de 0, el usuario si existe.
	 * 
	 * @param user el usuario a validar el login
	 * @param pass el pass del usuario a validar
	 * @return true si el usuario existe en la BD
	 */
	public static boolean validaLoginUsuario(String user, String pass){
		try{
			Statement st =con.createStatement();
			ResultSet rs = st.executeQuery("SELECT * FROM usuarios WHERE nombre = '" + user + "'");
			
			//si rs está vacío
			if(rs == null){
				TUI.printError("el usuario no existe o la contraseña no es válida");
				TUI.appControl();
				return false;
			}
			//si rs ha almacenado un resultado comparamos el password
			else{
				String password = "";
				while( rs.next() ){
					password = rs.getString("pass");
				}
				if(password.equals(pass)){
					return true;
				}
				else{
					TUI.printError("el usuario no existe o la contraseña no es válida");
					TUI.appControl();
					return false;
				}
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * Inserta un nuevo registro de 'ruta' dentro de la tabla 'rutas'.
	 * Sintaxis : 'insert into rutas (nombre,descripcion,desnivel,nombrex,user_id) values ('xx','xx','xx','xx','xx');'.
	 * 
	 * @return true si se ha tenido éxito, false en otro caso
	 */
	public static boolean insertIntoRutas(String user, String nombre, String descripcion, String desnivel, String distancia){
		
		try{
			// obtenemos la id del usuario para después hacer la inserción
			// select id from usuraios where nombre = 'emilio';
			Statement stIdUser =con.createStatement();
			ResultSet rsIdUser = stIdUser.executeQuery("select id from usuarios where nombre = '" + user +"'");
			int idUser = 0;
			if( rsIdUser != null ){
				while( rsIdUser.next() ){
					idUser = rsIdUser.getInt("id");
				}
			}
		
			//realizamos la insercion en la tabla
			String query = 
					"insert into rutas (nombre,descripcion,desnivel,distancia,nombrex,user_id) values (?,?,?,?,?,?)";
			PreparedStatement pSt = con.prepareStatement(query);
			
			pSt.setString(1, nombre);
			pSt.setString(2, descripcion);
			pSt.setDouble(3, Double.parseDouble(desnivel));
			pSt.setDouble(4, Double.parseDouble(distancia));
			pSt.setString(5, user);
			pSt.setInt(6, idUser);
			pSt.execute();	
				
			return true;
			
			
		}catch(SQLException e){
			e.printStackTrace();
		}
		
		return false;
	}
	
	/**
	 * Inserta un nuevo registro de 'documento' dentro de la tabla 'documentos'.
	 * Sintaxis : 'insert into documentos (nombre,id_ruta) values ('xx','xx');'.
	 * 
	 * Nombre de los ficheros : nombre del usuario+id_ruta+id_documento
	 * 
	 * @return true si se ha tenido éxito, false en otro caso
	 */
	public static boolean insertIntoDocumentos(String nombreDocumento, int id_ruta){
		
		try{
			
			String query = 
					"insert into documentos (nombre,id_ruta) values (?,?)";
			PreparedStatement pSt = con.prepareStatement(query);
			
			pSt.setString(1, nombreDocumento);
			pSt.setInt(2, id_ruta);
			pSt.execute();	
				
			return true;
			
		}catch(SQLException e){
			e.printStackTrace();
		}
	 
		return false;
	}
	
	
	/**
	 * @return la uĺtima id de docuemnto en la tabla
	 */
	public static int selectLastDocumentId(){
		int id=0;
		try{
			Statement st =con.createStatement();
			ResultSet rs = st.executeQuery("select last_value from documentos_id_seq");

			if( rs != null ){
				while( rs.next() ){
					id = rs.getInt("last_value");
				}
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		if(id == 1){
			return 0;
		}
		return id;
	}
	
	
	/**
	 * Imprime por pantalla las id y los nombres de todas las rutas.
	 * 
	 * @return id y nombre de todas las rutas
	 */
	public static void selectIdAndNameFromRutas(){
		ArrayList<Resumen> resumenes = new ArrayList<Resumen>();
		try{
			Statement st =con.createStatement();
			ResultSet rs = st.executeQuery("select id, nombre from rutas");
			
			Resumen resumen = null;
			
			if( rs != null ){
				while( rs.next() ){
					int id = rs.getInt("id");
					String nombre = rs.getString("nombre");
					resumen = new Resumen(id, nombre, "", 0, 0, "", 0);
					resumenes.add(resumen);
				}
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		
		if(resumenes.size() == 0){
			TUI.printError("aún no hay rutas registradas");
			return;
		}
		
		System.out.println("\nRutas: ");
		for(Resumen r : resumenes){
			System.out.println("\tid: " + r.getId() + " -> nombre: " + r.getNombre());
		}
		System.out.println();
	}
	
	
	/**
	 * 
	 * @param ruta la ruta a obtener la id
	 * @return la id de la ruta
	 */
	public static int selectIdRuta(String nombreRuta){
		int id=0;
		try{
			Statement st =con.createStatement();
			ResultSet rs = st.executeQuery(
					"select id from rutas where nombre = '" + nombreRuta + "' order by id desc limit 1");
			if( rs != null ){
				while( rs.next() ){
					id = rs.getInt("id");
				}
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return id;
	}
	
	
	/**
	 * Imprime el usuario que ha subido más Km de rutas
	 * 
	 * @returnvoid
	 */
	public static void selectUsuarioConMasKM(){

		try{
			//obtenemos los nombres de los usuarios
			ArrayList<String> nombresUsers = new ArrayList<String>();
			Statement stnombresUsers =con.createStatement();
			ResultSet rsNombresUsers = stnombresUsers.executeQuery("select nombre from usuarios");
			if( rsNombresUsers != null ){
				while( rsNombresUsers.next() ){
					nombresUsers.add(rsNombresUsers.getString("nombre"));
				}
			}
			
			//creamos un array para acumular Km
			double[] arrayKm = new double[nombresUsers.size()];
			
			Statement st =con.createStatement();
			//descartamos ids de rutas repetidas
			ResultSet rs = st.executeQuery("select distinct(id_ruta), nombre,id from documentos");
			
			//del resultSet obtenemos el nombre y lo almacenamos en un array
			ArrayList<Documento> documentos = new ArrayList<Documento>();
			if( rs != null ){
				while( rs.next() ){
					String nombreDoc = rs.getString("nombre");
					int idDocumento = rs.getInt("id");
					//extremos el nombre del usuario y la id de ruta
					String[] arrayNombreDoc = nombreDoc.split("_");
					String nombreUser = arrayNombreDoc[0];
					int idRuta = Integer.parseInt(arrayNombreDoc[1]);
					int idDoc = Integer.parseInt(arrayNombreDoc[2]);
					
					Documento doc = new Documento(idRuta, idDocumento, nombreUser);
					documentos.add(doc);
				}
			}
			
			//recorremos los usuarios
			double kmUser = 0;
			for(int i=0;i<nombresUsers.size();i++){
				kmUser =0;
				String nombreUser = nombresUsers.get(i);
				//por cada usuario recorremos todos los documentos
				for(int j=0;j<documentos.size();j++){
					//obtenemos la info
					String nombre = documentos.get(j).getNombre();
					int idRuta = documentos.get(j).getIdRuta();
					//obtenemos los km del usuario
					if(nombre.equals(nombreUser)){
						Statement stDistancia =con.createStatement();
						//descartamos ids de rutas repetidas
						ResultSet rsDistancia = stDistancia.executeQuery(
								"select distancia, id from rutas where id ="+idRuta+"");
						if( rsDistancia != null ){
							double distancia = 0;
							while( rsDistancia.next() ){
								distancia = rsDistancia.getDouble("distancia");
							}
							kmUser = kmUser + distancia;
						}
					}
				}
				arrayKm[i]= kmUser;
			}
			//buscamos el usuario con mayor valor
			Double max = Double.MIN_VALUE;
			String nombreRuta="";
			int posicionMaxDistancia = 0;
			//calculamos la distancia mas larga
			for(int i =0;i<arrayKm.length;i++){
				double distancia = arrayKm[i];
				if(distancia > max){
					max = distancia;
					posicionMaxDistancia = i;
				}
			}
			if(nombresUsers.size() == 0){
				TUI.printError("aún no hay usuarios registrados");
			}
			else{
				System.out.println("\n Usuario com mas Km acumulados :\n");
				System.out.println("\t"+nombresUsers.get(posicionMaxDistancia) +" : " + max + " km");
			}
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	
	/**
	 * El nombre de usuarios almacenados en la tabla usuarios.
	 * 
	 * @return todos los nombres de usuarios
	 */
	public static ArrayList<String> countusuarios(){
		ArrayList<String> usuarios = new ArrayList<String>();
		try{
			Statement st =con.createStatement();
			ResultSet rs = st.executeQuery("select nombre from usuarios");
			if( rs != null ){
				while( rs.next() ){
					usuarios.add(rs.getString("nombre"));
				}
			}
		}
		catch(SQLException e){
			e.printStackTrace();
		}	
		return usuarios;
	}
	
	
	/**
	 * Dada una id busca en la BD una ruta.
	 * 
	 * @param idRuta a buscar en la BD
	 * @return el resumen de la ruta
	 */
	public static Resumen selectAllFromRutas(int idRuta){
		Resumen resumen = null;
		try{
			Statement st =con.createStatement();
			ResultSet rs = st.executeQuery("select * from rutas where id = '"+idRuta+"'");
			if( rs != null ){
				while( rs.next() ){
					int id = rs.getInt("id");
					String nombre = rs.getString("nombre");
					String descripcion = rs.getString("descripcion"); 
					double desnivel = rs.getDouble("desnivel");
					double distancia = rs.getDouble("distancia");
					String nombreUsuario = rs.getString("nombrex");
					int idUsuario = rs.getInt("user_id");
					resumen = new Resumen(id, nombre, descripcion, desnivel, distancia, nombreUsuario, idUsuario);
				}
			}
			return resumen;
		}
		catch(SQLException e){
			e.printStackTrace();
		}	
		return resumen;
	}
	
	
	/**
	 * Dado un usuario, selecciona todas sus rutas.
	 * 
	 * @param user el usuario a buscar sus rutas
	 * @return todas las rutas del usuario
	 */
	public static ArrayList<Resumen> selectAllRutasFromUsuario(String user){
		
		ArrayList<Resumen> resumenes = new ArrayList<Resumen>();
		Resumen resumen = null;
		try{
			Statement st =con.createStatement();
			ResultSet rs = st.executeQuery("select * from rutas where nombrex = '"+user+"'");
			if( rs != null ){
				while( rs.next() ){
					int id = rs.getInt("id");
					String nombre = rs.getString("nombre");
					String descripcion = rs.getString("descripcion"); 
					double desnivel = rs.getDouble("desnivel");
					double distancia = rs.getDouble("distancia");
					String nombreUsuario = rs.getString("nombrex");
					int idUsuario = rs.getInt("user_id");
					resumen = new Resumen(id, nombre, descripcion, desnivel, distancia, nombreUsuario, idUsuario);
					resumenes.add(resumen);
				}
			}
			return resumenes;
		}
		catch(SQLException e){
			e.printStackTrace();
		}	
		return resumenes;
	}
	
	
	/**
	 * Selecciona todas las rutas en la DB gpxdb.
	 * 
	 * @return todas las rutas almacenadas
	 */
	public static ArrayList<Resumen> selectAllrutas(){
		
		ArrayList<Resumen> resumenes = new ArrayList<Resumen>();
		Resumen resumen = null;
		try{
			Statement st =con.createStatement();
			ResultSet rs = st.executeQuery("select * from rutas");
			if( rs != null ){
				while( rs.next() ){
					int id = rs.getInt("id");
					String nombre = rs.getString("nombre");
					String descripcion = rs.getString("descripcion"); 
					double desnivel = rs.getDouble("desnivel");
					double distancia = rs.getDouble("distancia");
					String nombreUsuario = rs.getString("nombrex");
					int idUsuario = rs.getInt("user_id");
					resumen = new Resumen(id, nombre, descripcion, desnivel, distancia, nombreUsuario, idUsuario);
					resumenes.add(resumen);
				}
			}
			return resumenes;
		}
		catch(SQLException e){
			e.printStackTrace();
		}	
		return resumenes;
	}
	
	
	/**
	 * 
	 * @param ruta la ruta a obtener la id
	 * @return void
	 */
	public static void selectUsuarioConMasDesnivel(){
		
		try{
			//obtenemos los nombres de los usuarios
			ArrayList<String> nombresUsers = new ArrayList<String>();
			Statement stnombresUsers =con.createStatement();
			ResultSet rsNombresUsers = stnombresUsers.executeQuery("select nombre from usuarios");
			if( rsNombresUsers != null ){
				while( rsNombresUsers.next() ){
					nombresUsers.add(rsNombresUsers.getString("nombre"));
				}
			}
			
			//creamos un array para acumular Km
			double[] arrayKm = new double[nombresUsers.size()];
			
			Statement st =con.createStatement();
			//descartamos ids de rutas repetidas
			ResultSet rs = st.executeQuery("select distinct(id_ruta), nombre,id from documentos");
			
			//del resultSet obtenemos el nombre y lo almacenamos en un array
			ArrayList<Documento> documentos = new ArrayList<Documento>();
			if( rs != null ){
				while( rs.next() ){
					String nombreDoc = rs.getString("nombre");
					int idDocumento = rs.getInt("id");
					//extremos el nombre del usuario y la id de ruta
					String[] arrayNombreDoc = nombreDoc.split("_");
					String nombreUser = arrayNombreDoc[0];
					int idRuta = Integer.parseInt(arrayNombreDoc[1]);
					int idDoc = Integer.parseInt(arrayNombreDoc[2]);
					
					Documento doc = new Documento(idRuta, idDocumento, nombreUser);
					documentos.add(doc);
				}
			}
			
			//recorremos los usuarios
			double kmUser = 0;
			for(int i=0;i<nombresUsers.size();i++){
				kmUser =0;
				String nombreUser = nombresUsers.get(i);
				//por cada usuario recorremos todos los documentos
				for(int j=0;j<documentos.size();j++){
					//obtenemos la info
					String nombre = documentos.get(j).getNombre();
					int idRuta = documentos.get(j).getIdRuta();
					//obtenemos los km del usuario
					if(nombre.equals(nombreUser)){
						Statement stDistancia =con.createStatement();
						//descartamos ids de rutas repetidas
						ResultSet rsDistancia = stDistancia.executeQuery(
								"select desnivel, id from rutas where id ="+idRuta+"");
						if( rsDistancia != null ){
							double distancia = 0;
							while( rsDistancia.next() ){
								distancia = rsDistancia.getDouble("desnivel");
							}
							kmUser = kmUser + distancia;
						}
					}
				}
				arrayKm[i]= kmUser;
			}
			//buscamos el usuario con mayor valor
			Double max = Double.MIN_VALUE;
			String nombreRuta="";
			int posicionMaxDistancia = 0;
			//calculamos la distancia mas larga
			for(int i =0;i<arrayKm.length;i++){
				double distancia = arrayKm[i];
				if(distancia > max){
					max = distancia;
					posicionMaxDistancia = i;
				}
			}
			
			if(nombresUsers.size() == 0){
				TUI.printError("aún no hay usuarios registrados");
				return;
			}
			else{
				System.out.println("\n Usuario com mas desnivel acumulado :\n");
				System.out.println("\t"+nombresUsers.get(posicionMaxDistancia) +" : " + max/1000 + " km");
			}
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	
	/**
	 * selecciona la ruta más corta
	 * Sintaxis: select nombre, distancia from rutas;
	 * 
	 * @return void
	 */
	public static void selectRutaMasCorta(){

		ArrayList<Resumen> resumenes = new ArrayList<Resumen>();
		try{
			Statement st =con.createStatement();
			ResultSet rs = st.executeQuery("select nombre, distancia from rutas");

			if( rs != null ){
				while( rs.next() ){
					int id = 0;
					String nombre = rs.getString("nombre");
					String descripcion = ""; 
					double desnivel = 0;
					double distancia = rs.getDouble("distancia");
					String nombreUsuario = "";
					int idUsuario = 0;
					Resumen resumen = new Resumen(id, nombre, descripcion, desnivel, distancia, nombreUsuario, idUsuario);
					resumenes.add(resumen);
				}
			}
			Double min = Double.MAX_VALUE;
			String nombreRuta="";
			//calculamos la distancia mas corta
			for(Resumen r : resumenes){
				double distanciaR = r.getDistancia();
				if(distanciaR <= min){
					min = distanciaR;
					nombreRuta = r.getNombre();
				}
			}
			if(resumenes.size() == 0){
				TUI.printError("aún no hay rutas registradas");
				return;
			}
			
			System.out.println("\n Ruta mas corta:\n");
			System.out.println("\t"+nombreRuta +" : " + min + "km");
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	/**
	 * selecciona la ruta más larga
	 * Sintaxis: select nombre, distancia from rutas;
	 * 
	 * @return void
	 */
	public static void selectRutaMasLarga(){

		ArrayList<Resumen> resumenes = new ArrayList<Resumen>();
		try{
			Statement st =con.createStatement();
			ResultSet rs = st.executeQuery("select nombre, distancia from rutas");

			if( rs != null ){
				while( rs.next() ){
					int id = 0;
					String nombre = rs.getString("nombre");
					String descripcion = ""; 
					double desnivel = 0;
					double distancia = rs.getDouble("distancia");
					String nombreUsuario = "";
					int idUsuario = 0;
					Resumen resumen = new Resumen(id, nombre, descripcion, desnivel, distancia, nombreUsuario, idUsuario);
					resumenes.add(resumen);
				}
			}
			Double max = Double.MIN_VALUE;
			String nombreRuta="";
			//calculamos la distancia mas larga
			for(Resumen r : resumenes){
				double distanciaR = r.getDistancia();
				if(distanciaR > max){
					max = distanciaR;
					nombreRuta = r.getNombre();
				}
			}
			
			if(resumenes.size() == 0){
				TUI.printError("aún no hay rutas registradas");
				return;
			}
			
			System.out.println("\n Ruta mas larga:\n");
			System.out.println("\t"+nombreRuta +":  " + max + "km");
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	/**
	 * Vacía por completo la DB eliminando el contenido de todas las tablas y la restaura 
	 * a los valores por defecto.
	 * 
	 * @return void
	 */
	public static void deletePostgresGpxDB(){
		Statement st;
		try {
			st = con.createStatement();
			
			st.executeUpdate("delete from documentos");
			st.executeUpdate("delete from rutas");
			st.executeUpdate("delete from usuarios");
			
			st.executeQuery("SELECT pg_catalog.setval('documentos_id_seq', 1, false)");
			st.executeQuery("SELECT pg_catalog.setval('rutas_id_seq', 1, false)");
			st.executeQuery("SELECT pg_catalog.setval('usuarios_id_seq', 1, false)");
			
		} 
		catch (SQLException e) {
			e.printStackTrace();
		} 
	}
	
	
	/**
	 * Establece una nueva conexión al servidor de postgresQl y en concreto
	 * a la BD 'gpxdb'.
	 * 
	 * @param connection el objeto Connection ya instanciado
	 */
	public static void setPostgresConnection(Connection connection){
		con = connection;
	}
}
