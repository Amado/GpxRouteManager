package postgresDAO;

public class Documento {
	
	int idRuta;
	int idDoc;
	String nombreUser;
	
	public Documento(int idRuta, int idDoc, String nombreDoc) {
		super();
		this.idRuta = idRuta;
		this.idDoc = idDoc;
		this.nombreUser = nombreDoc;
	}

	public int getIdRuta() {
		return idRuta;
	}

	public void setIdRuta(int idRuta) {
		this.idRuta = idRuta;
	}

	public int getIdDoc() {
		return idDoc;
	}

	public void setIdDoc(int idDoc) {
		this.idDoc = idDoc;
	}

	public String getNombre() {
		return nombreUser;
	}

	public void setNombre(String nombre) {
		this.nombreUser = nombre;
	}

	@Override
	public String toString() {
		return "Documento [idRuta=" + idRuta + ", idDoc=" + idDoc
				+ ", nombreDoc=" + nombreUser + "]";
	}

}
