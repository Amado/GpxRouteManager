package postgresDAO;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Lee un fichero xxx.gpx del que extrae la siguiente info :
 * 
 * 		-> Nombre de la ruta 
 * 		-> Descripción de la ruta 
 * 		-> Desnivel total positivo 
 * 		-> Distancia total recorrida
 * 
 * @author Emilio Amado 2015
 *
 */
public class LeeResumenDOM {
	
	private static NodeList nodeList;
	private static String nombreRuta;
	private static String descripcionRuta;
	private static String desnivelPositivo;
	private static String distancia;

	/**
	 * Realiza un resumen del fichero xxx.gpx con la info
	 * descrita en la cabecera de la clase.
	 * 
	 * @param resumenRuta ruta absoluta del fichero a resumir
	 */
	public static void LeeResumen(String resumenRuta) {
//		System.out.println("LeeResumen.class : "+resumenRuta);
		try {

			File xml = new File("resumenesRutas/" + resumenRuta);

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();

			Document doc = dbFactory.newDocumentBuilder().parse(xml);
			
			doc.getDocumentElement().normalize();
			//capturamos la lsita de nodos
			nodeList = doc.getElementsByTagName("resumen");
			Node mainNode = nodeList.item(0);
			NodeList nodeChilds = mainNode.getChildNodes();
			
			//recorremos la lista
			for(int i=0;i<nodeChilds.getLength();i++){
				
				Node node = nodeChilds.item(i);
				if(node.getNodeType() == Node.ELEMENT_NODE){
					if(node.getNodeName().equals("ruta_nombre")){
						Element e = (Element) node;
						nombreRuta = e.getAttribute("nombre");
					}
					else if(node.getNodeName().equals("descripcion")){
						descripcionRuta = node.getTextContent();
					}
					else if(node.getNodeName().equals("desnivel_positivo")){
						desnivelPositivo = node.getTextContent();
					}
					else if(node.getNodeName().equals("distancia_total")){
						distancia = node.getTextContent();
					}
				}
			}
//			System.out.println(nombreRuta+"\n\n");
//			System.out.println(descripcionRuta+"\n\n");
//			System.out.println(desnivelPositivo+"\n\n");
//			System.out.println(distancia+"\n\n");
			
			
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Una vez procesado el fichero xxx.gpx original, devuelve el NodeValue del
	 * att 'nombre' del nodo 'ruta_nombre'
	 * 
	 * @return el nombre de la ruta
	 */
	public static  String getNombreRuta() {
		return nombreRuta;
	}

	/**
	 * Una vez procesado el fichero xxx.gpx original, devuelve el TextContent
	 * del nodo 'descripcion'
	 * 
	 * @return descripción de la ruta
	 */
	public static  String getDescripcionRuta() {
		return descripcionRuta;
	}

	/**
	 * Una vez procesado el fichero xxx.gpx original, devuelve el TextContent
	 * del nodo 'desnivel_positivo'
	 * 
	 * @return desnivel positivo toal de la ruta
	 */
	public static  String getDesnivelPositivoRuta() {
		return desnivelPositivo;
	}

	/**
	 * Una vez procesado el fichero xxx.gpx original, devuelve el TextContent
	 * del nodo 'distnacia_total'
	 * 
	 * @return distancia total recorrida en la ruta
	 */
	public static  String getDistanciaTotalRuta() {
		return distancia;
	}
}
