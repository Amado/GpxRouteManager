package xsltTransformer;

import java.io.File;
import java.io.IOException;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

/**
 * La clase contiene los métodos necesarios para convertir una ruta con extension
 * gpx o xml en un fichero html.
 * 
 * @author Emilio Amado 2015
 *
 */
public class XsltRutas {

	/**
	 * Recoge un fichero xml de la carpeta basexFiles para convertirlo en
	 * un fichero html y guardarlo en la carpeta html, gracias
	 * a la plantilla xsl 'plantillaRutaCompleta.xsl'  
	 * <p>El fichero html se genera en la carpeta : /home/userName/MisRutasAppHtmlFiles.
	 * 
	 * @param source el fichero xml de entrada
	 * @param output el fichero html de salida
	 */
	public static void xsltRutaCompleta(String source, String output) {

		try {
			File sf = new File("basexFiles/" + source); // source file
			File rf = new File(System.getProperty("user.home")+"/MisRutasAppHtmlFiles/" + output); // result file
			File tf = new File("xsl/plantillaRutaCompleta.xsl"); // template
																	// file
			TransformerFactory f = TransformerFactory.newInstance();
			Transformer t = f.newTransformer(new StreamSource(tf));
			Source s = new StreamSource(sf);
			Result r = new StreamResult(rf);
			t.transform(s, r);
			System.out.println("\nhtml con la ruta completa generado en :");
			System.out.println("\t/home/userName/MisRutasAppHtmlFiles\n");
			
			Runtime rt = Runtime.getRuntime();
			try {
				rt.exec("/usr/bin/firefox " + System.getProperty("user.home")+"/MisRutasAppHtmlFiles/" + output);
			} catch (IOException e) {
				System.out.println("Error, firefox no se encuentra en : /usr/bin/firefox");
			}
			
		} catch (TransformerConfigurationException e) {
//			System.out.println(e.toString());
		} catch (TransformerException e) {
//			System.out.println(e.toString());
		}
	}

	/**
	 * Recoge un fichero xml de la carpeta resumenesRutas para convertirlo en
	 * un fichero html y guardarlo en la carpeta html, gracias
	 * a la plantilla xsl 'plantillaTodasRutasUsuario.xsl'  
	 * <p>El fichero html se genera en la carpeta : /home/userName/MisRutasAppHtmlFiles.
	 * 
	 * @param source el fichero xml de entrada
	 * @param output el fichero html de salida
	 */
	public static void xsltTodasRutasUsuario(String source, String output) {
		try {
			File sf = new File("resumenesRutas/" + source); // source file
			File rf = new File(System.getProperty("user.home")+"/MisRutasAppHtmlFiles/" + output); // result file
			File tf = new File("xsl/plantillaTodasRutasUsuario.xsl"); // template
																	// file
			TransformerFactory f = TransformerFactory.newInstance();
			Transformer t = f.newTransformer(new StreamSource(tf));
			Source s = new StreamSource(sf);
			Result r = new StreamResult(rf);
			t.transform(s, r);
			System.out.println("\nhtml con la ruta completa generado en :");
			System.out.println("\t/home/userName/MisRutasAppHtmlFiles\n");
			
			Runtime rt = Runtime.getRuntime();
			try {
				rt.exec("/usr/bin/firefox " + System.getProperty("user.home")+"/MisRutasAppHtmlFiles/" + output);
			} catch (IOException e) {
				System.out.println("Error, firefox no se encuentra en : /usr/bin/firefox");
			}
			System.out.println("dsfsdfsdfsf");
		} catch (TransformerConfigurationException e) {
//			System.out.println(e.toString());
		} catch (TransformerException e) {
//			System.out.println(e.toString());
		}
	}
	

	/**
	 * Recoge un fichero xml de la carpeta resumenesRutas para convertirlo en
	 * un fichero html y guardarlo en la carpeta html, gracias
	 * a la plantilla xsl 'plantillaTodasRutas.xsl'  
	 * <p>El fichero html se genera en la carpeta : /home/userName/MisRutasAppHtmlFiles.
	 * 
	 * @param source el fichero xml de entrada
	 * @param output el fichero html de salida
	 */
	public static void xsltTodasRutas(String source, String output) {
		try {
			File sf = new File("resumenesRutas/" + source); // source file
			File rf = new File(System.getProperty("user.home")+"/MisRutasAppHtmlFiles/" + output); // result file
			File tf = new File("xsl/plantillaTodasRutas.xsl"); // template
																	// file
			TransformerFactory f = TransformerFactory.newInstance();
			Transformer t = f.newTransformer(new StreamSource(tf));
			Source s = new StreamSource(sf);
			Result r = new StreamResult(rf);
			t.transform(s, r);
			System.out.println("\nhtml con la ruta completa generado en :");
			System.out.println("\t/home/userName/MisRutasAppHtmlFiles\n");
			
			Runtime rt = Runtime.getRuntime();
			try {
				rt.exec("/usr/bin/firefox " + System.getProperty("user.home")+"/MisRutasAppHtmlFiles/" + output);
			} catch (IOException e) {
				System.out.println("Error, firefox no se encuentra en : /usr/bin/firefox");
			}
			
		} catch (TransformerConfigurationException e) {
//			System.out.println(e.toString());
		} catch (TransformerException e) {
//			System.out.println(e.toString());
		}
	}
}
