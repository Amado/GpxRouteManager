package baseXServer;

import java.io.IOException;

import org.basex.BaseXServer;

/**
 * Inicializa el servidor de BaseX en un nuevo Thread y lo mantiene
 * abierto hasta nueva orden.
 * 
 * @author Emilio Amado 2015
 * @see http://docs.basex.org/javadoc/org/basex/BaseXServer.html
 *
 */
public class MyBaseXServer implements Runnable{
	
	public static boolean stopBasexServer = false;
	
	/**
	 * Inicializa el servidor
	 */
	@Override
	public void run() {
		synchronized (this) {
			BaseXServer server = null;
			try{
				server = new BaseXServer();
				while(!stopBasexServer){
					
				}
				server.stop();
			} catch(IOException e){
				e.printStackTrace();
			} finally{
				try {
					server.stop();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}		
	}

}
