package baseXServer;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.basex.api.client.ClientSession;
import org.basex.core.cmd.CreateDB;
import org.basex.core.cmd.DropDB;
import org.basex.core.cmd.Open;

import dbServersMain.TUI;


/**
 * Data Access Object para realizar operaciones sobre BaseX.
 * Es necesario arrancar el cliente en un nuevo Thread, para
 * permitir arrancar primero al servidor de Basex y después 
 * realizar la conexión del cliente de forma controlada.
 * 
 * @author Emilio Amado 2015
 * @see http://docs.basex.org/javadoc/org/basex/api/client/ClientSession.html
 *
 */
public class BasexDAO implements Runnable{

	private static ClientSession session;
	
	/**
	 * Obtiene los bytes de un fichero y lo almacena en la
	 * BD de baseX.
	 * 
	 * @param filePath la ruta al fichero
	 * @return true si el fichero existe
	 */
	public static boolean alamcenaFichero(String filePath){
		
		//obtenemos el nombre del fichero para almacenarlo con el mismo nombre
		String[] arrayFilePath = filePath.split("/");
		String fileName = arrayFilePath[arrayFilePath.length-1];
		
		try {
			Path path = Paths.get(filePath);
			InputStream is = new ByteArrayInputStream(Files.readAllBytes(path));
			session.store(fileName, is);
			return true;
		} catch (IOException e) {
			TUI.printError("el fichero no existe o ruta mal especificada");
//			e.printStackTrace();
			return false;
		}
	}
	
	
	/**
	 * Recupera un fichero de la BD baseX para poder procesarlo.
	 * 
	 * @param file el fichero a recuperar
	 * @return el FileOutputStream generado
	 */
	public static FileOutputStream recuperaFichero(String file){
		FileOutputStream fileout = null;
		try {
			//obtenemos el nombre del fichero para almacenarlo con el mismo nombre
			String[] arrayFilePath = file.split("/");
			String fileName = arrayFilePath[arrayFilePath.length-1];
			
		    // receive data
		    ByteArrayOutputStream baos = new ByteArrayOutputStream();
			session.setOutputStream(baos);
			session.execute("retrieve " + fileName);
			fileout = new FileOutputStream("basexFiles/"+fileName);
			baos.writeTo(fileout);
			session.setOutputStream(System.out);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return fileout;
	}
	
	/**
	 * Elimina la BD de baseX.
	 */
	public static void dropDB(){
		try {
			session.execute(new DropDB("basexDB"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Imprime el contenido de la db.
	 */
	public static void printRutasContent(){
		try {
//			System.out.println( session.execute("xquery collection('basexDB')"));
			System.out.println("\n" + session.execute("list basexDB"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	/**
	 * Añade a la BD de baseX el resumen de una ruta.
	 * 
	 * @param filePath la ruta del fichero xml a añadir
	 */
	public static void addToBasex(String filePath){
		//obtenemos el nombre del fichero para almacenarlo con el mismo nombre
		String[] arrayFilePath = filePath.split("/");
		String fileName = arrayFilePath[arrayFilePath.length-1];

		try {
			//cargamos el fichero xml en un InputStream
			Path path = Paths.get(filePath);
			InputStream is = new ByteArrayInputStream(Files.readAllBytes(path));
			//añadimos a la DB el fichero xml
			//equivalente a cargar una tabla en la DB
			session.add(fileName, //nombre que le damos dentro de la DB
						is);//contenido a añadir a la db
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Elimina la DB de baseX y la vuelve a crear vacía.
	 * 
	 * @return void
	 */
	public static void deleteBasexDB(){
		try {
			session.execute(new DropDB("basexDB"));
			session.execute(new CreateDB("basexDB"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Inicializa el cliente para BaseX y la BD.
	 */
	@Override
	public void run() {
		try {
			session = new ClientSession("localhost", 1984, "admin", "admin");
			
			session.execute(new Open("basexDB"));
			
//			session.execute(new CreateDB("basexDB"));
			
			while(true){
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			try {
				session.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
