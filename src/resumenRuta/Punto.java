package resumenRuta;


/**
 * Modela el punto de una coordenada con latitud, longitud y elevación.
 * 
 * @author Emilio Amado 2015
 *
 */
public class Punto {
	
	double latitud;
	double longitud;
	double elevacion;
	
	public Punto(double latitud, double longitud, double elevacion) {
		super();
		this.latitud = latitud;
		this.longitud = longitud;
		this.elevacion = elevacion;
	}

	public double getLatitud() {
		return latitud;
	}

	public void setLatitud(double latitud) {
		this.latitud = latitud;
	}

	public double getLongitud() {
		return longitud;
	}

	public void setLongitud(double longitud) {
		this.longitud = longitud;
	}

	public double getElevacion() {
		return elevacion;
	}

	public void setElevacion(double elevacion) {
		this.elevacion = elevacion;
	}

	@Override
	public String toString() {
		return "Punto [latitud=" + latitud + ", longitud=" + longitud
				+ ", elevacion=" + elevacion + "]";
	}
}
