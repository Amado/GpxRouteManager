package resumenRuta;

import java.util.ArrayList;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


/**
 * Clase que hereda de DefaultHandler para la lectura de un fichero xxx.gpx
 * y realizar los siguientes cáculos :
 * 		-> Distancia total recorrida.
 * 		-> Desnivel total positivo.
 * 		-> Desnivel total negativo.
 * 		-> Desnivel total acumulado.
 * 	  	-> Tiempo en realizar la ruta (milis).
 * 
 * @see DefaultHandler
 * @author Emilio Amado 2015
 *
 */
public class MyHandler extends DefaultHandler {
	
	 long tiempoInicio;
	 long tiempoFinal;
	 double desnivelAcumulado = 0;
	 double desnivelTotalPositivo = 0;
	 double desnivelTotalNegativo = 0;
	 double desnivel1 = 0;
	 double desnivel2 = 0;
	 static double distanciaRecorrida = 0;
	 int totalPuntos = 0;
	 double latitud1 = 0;
	 double longitud1 = 0;
	 double latitud2 = 0;
	 double longitud2 = 0;
	 boolean ele = false;
	 Punto punto = null;
	 ArrayList<Punto> puntos = new ArrayList<Punto>();

	 /**
	  * @see DefaultHandler
	  */
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		
		if(qName.equals("trkseg")){
			tiempoInicio = System.currentTimeMillis();
		}
		
		if(qName.equals("trkpt")){
			punto = new Punto(0, 0, 0);
			totalPuntos ++;
			double latitudDouble = Math.toRadians(Double.parseDouble(attributes.getValue(0)));
			double longitudDouble = Math.toRadians(Double.parseDouble(attributes.getValue(1)));
			
			punto.setLatitud(latitudDouble);
			punto.setLongitud(longitudDouble);
		}
		
		if(qName.equals("ele")){
			ele = true;
		}
	}
	
	//================================================================
	
	 /**
	  * @see DefaultHandler
	  */
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		
		if(qName.equals("trkseg")){
			tiempoFinal = System.currentTimeMillis();
			procesaResultados();
		}
		
		if(qName.equals("trkpt")){
			puntos.add(punto);
		}
		
	}
	
	//=====================================================
	
	 /**
	  * @see DefaultHandler
	  */
	public void characters(char ch[], int start, int length)
			throws SAXException {
		
		if(ele){
			double eleDouble = Double.parseDouble(new String(ch, start, length));
			punto.setElevacion(eleDouble);		
			ele = false;
		}
		
	}
	
	//==============================
	
	/**
	 * Una vez obtenida por SAX la info necesaria de cada nodo reliza los
	 * cálculos descritos en la cabecera de la clase.
	 */
	public void procesaResultados(){
		
		for(int i=0;i<puntos.size()-1;i++){
			Punto p1 = puntos.get(i);
			Punto p2 = puntos.get(i+1);
			
			desnivel1 = p1.getElevacion();
			desnivel2 = p2.getElevacion();
			
			latitud1 = p1.getLatitud();
			latitud2 = p2.getLatitud();
			
			longitud1 = p1.getLongitud();
			longitud2 = p2.getLongitud();
			
			/*
			 * radio terrestre = 6371
			 * a=sin(distanciaLatitud/2)^2 + (sin(distanciaLongitud/2)^2)*cos(latitudOrigen)*cos(latitudDestino ) 
			Distancia = radioTerrestre* (2*atan2(sqrt(a),sqrt(1-a) ) )
			 */
			
			double a = Math.pow(Math.sin((latitud1-latitud2)/2),2) +
					Math.pow(Math.sin((longitud1-longitud2)/2),2) *
					Math.cos(latitud1) * Math.cos(latitud2);
			
			double distancia = 6371 * (2*Math.atan2( Math.sqrt(a),Math.sqrt(1-a)));
			//System.out.println("distancia entre puntos  : " + distancia);
			
			distanciaRecorrida = distanciaRecorrida + distancia;
			
			if(desnivel1 >= desnivel2){
				double desnivelPositivo = desnivel1 - desnivel2;
				desnivelTotalPositivo += desnivelPositivo;
			}
			else if(desnivel1 < desnivel2){
				double desnivelNegativo = desnivel1 - desnivel2;
				desnivelTotalNegativo += desnivelNegativo;
			}
		}
		desnivelAcumulado = desnivelTotalPositivo - desnivelTotalNegativo;
	}
	
	//==============================
	public void muestraResultados(){
		System.out.println("\n");
		System.out.println("tiempo total : " + (tiempoFinal-tiempoInicio));
		System.out.println("total de puntos : " + totalPuntos);
		System.out.println("desnivel positivo : " + desnivelTotalPositivo);
		System.out.println("desnivel negativo : " + desnivelTotalNegativo);
		System.out.println("desnivel acumulado : " + desnivelAcumulado);
		System.out.println("distancia total recorrida : " + distanciaRecorrida);
	}
	
	public static double distanciaTotal(){
		return distanciaRecorrida;
	}
}
