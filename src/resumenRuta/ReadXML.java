package resumenRuta;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.SAXException;

import dbServersMain.GPX;
import dbServersMain.TUI;

/**
 * Carga un Objeto DefaultHandler para la lectura de un fichero xml.
 * 
 * @author Emilio Amado 2015
 * @see DefaultHandler
 *
 */
public class ReadXML {
	
	//public static void main(String argv[]) {
	
	/**
	 * Inicia la lectura a un fichero xml.
	 * 
	 * @see DefaultHandler
	 * @param rutaFileName el fichero a leer, ruta al directorio incluida
	 */
	public static void iniciaLecturaSAX(String user, String rutaFileName){

		try {
			//instanciamos un nuevo SAXParser
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser = factory.newSAXParser();
			//parseamos el fichero xml con nuestro Handler
			MyHandler handler = new MyHandler();
			saxParser.parse(rutaFileName, handler);
			
			//handler.muestraResultados();
			
		} catch (ParserConfigurationException e) {
			TUI.printError("fichero xml mal formado");
			GPX.successOnSubirRuta = false;
			GPX.almacenaAccionEnDb4o(user, 1, GPX.successOnSubirRuta);
			TUI.appControl();
		} catch (SAXException e) {
			TUI.printError("fichero xml mal formado");
			GPX.successOnSubirRuta = false;
			GPX.almacenaAccionEnDb4o(user, 1, GPX.successOnSubirRuta);
			TUI.appControl();
		} catch (IOException e) {
			TUI.printError("el fichero no existe");
			GPX.successOnSubirRuta = false;
			GPX.almacenaAccionEnDb4o(user, 1, GPX.successOnSubirRuta);
			TUI.appControl();
		}
	}
}
