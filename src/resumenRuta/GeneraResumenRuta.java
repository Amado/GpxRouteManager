package resumenRuta;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

import postgresDAO.Resumen;

/**
 * Clase encargada de generar un resumen de un fichero xxx.gpx
 * <p>El resumen contiene los siguientes elementos :
 * 		-> Nombre de la ruta.
 * 		-> Descripción.
 * 		-> Distancia total recorrida.
 * 		-> Desnivel total positivo.
 * 		-> Desnivel total negativo.
 * 		-> Desnivel total acumulado.
 * 	  	-> Tiempo en realizar la ruta (milis).
 * 
 * @author Emilio Amado 2015
 *
 */
public class GeneraResumenRuta {

	static long tiempoInicio;
	static long tiempoFinal;
	static double desnivelAcumulado = 0;
	static double desnivelTotalPositivo = 0;
	static double desnivelTotalNegativo = 0;
	static double desnivel1 = 0;
	static double desnivel2 = 0;
	static double distanciaTotal = 0;

	
	/**
	 * Genera el resumen de la ruta xxx.gpx.
	 * 
	 * @param rutaFileName el nombre del fichero a procesar ruta al directorio incluida
	 * @param resumenRutaFileName el nombre del fichero resultante sin ruta al directorio
	 * @return true si se tiene éxito al generar el resumen, false en otro caso
	 */
	public static boolean generaResumen(String user, String rutaFileName, String resumenRutaFileName) {
//		System.out.println("ruta fichero : " + rutaFileName);
//		System.out.println("nombre resumen : " + resumenRutaFileName);

		try {
			// iniciamos una instancia de DocumentBuilder
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = dbf.newDocumentBuilder();
			Document doc1 = docBuilder.newDocument();
			
			ReadXML.iniciaLecturaSAX(user, rutaFileName);

			distanciaTotal = MyHandler.distanciaTotal();

			Element rootElement = doc1.createElement("resumen");
			doc1.appendChild(rootElement);

			File xml = new File(rutaFileName);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();

			Document doc = dbFactory.newDocumentBuilder().parse(xml);

			doc.getDocumentElement().normalize();

			NodeList nombres = doc.getElementsByTagName("name");
			Node nodoNombre = nombres.item(0);
			String valorNombre = nodoNombre.getTextContent();
			//System.out.println(valorNombre);

			Element ruta_nombre = doc1.createElement("ruta_nombre");
			rootElement.appendChild(ruta_nombre);

			Attr nombreAtt = doc1.createAttribute("nombre");
			nombreAtt.setValue(valorNombre);
			ruta_nombre.setAttributeNode(nombreAtt);

			NodeList descripciones = doc.getElementsByTagName("desc");
			Node nododescripcion = descripciones.item(0);
			String valordescripcion = nododescripcion.getTextContent();
			//System.out.println(valorNombre);

			Element descripcion = doc1.createElement("descripcion");
			Text descripcionText = doc1.createTextNode(valordescripcion);
			descripcion.appendChild(descripcionText);
			rootElement.appendChild(descripcion);

			NodeList puntos = doc.getElementsByTagName("trkpt");
			//System.out.println("total de puntos : " + puntos.getLength());
			for (int i = 0; i < puntos.getLength() - 1; i++) {
				Node punto1 = puntos.item(i);
				if (punto1.getNodeType() == Node.ELEMENT_NODE) {
					NodeList nodosPunto1 = punto1.getChildNodes();
					Node nodoElevacion1 = nodosPunto1.item(1);
					desnivel1 = Double.parseDouble(nodoElevacion1
							.getTextContent());

				}
				Node punto2 = puntos.item(i + 1);
				if (punto2.getNodeType() == Node.ELEMENT_NODE) {
					NodeList nodosPunto2 = punto2.getChildNodes();
					Node nodoElevacion2 = nodosPunto2.item(1);
					desnivel2 = Double.parseDouble(nodoElevacion2
							.getTextContent());
				}
				if (desnivel1 >= desnivel2) {
					double desnivelPositivo = desnivel1 - desnivel2;
					desnivelTotalPositivo += desnivelPositivo;
				} else if (desnivel1 < desnivel2) {
					double desnivelNegativo = desnivel1 - desnivel2;
					desnivelTotalNegativo += desnivelNegativo;
				}
			}
			tiempoFinal = System.currentTimeMillis();

//			System.out.println("desnivel positivo : " + desnivelTotalPositivo);
//			System.out.println("desnivel negativo : " + desnivelTotalNegativo);
//			System.out.println("desnivel acumulado : "
//					+ (desnivelTotalPositivo - desnivelTotalNegativo));
//			System.out.println("distancia total recorrida : " + distanciaTotal);
//			System.out.println("tiempo total : " + (tiempoFinal - tiempoInicio));

			Element distancia_total = doc1.createElement("distancia_total");
			Text distancia_totalText = doc1.createTextNode("" + distanciaTotal);
			distancia_total.appendChild(distancia_totalText);
			rootElement.appendChild(distancia_total);

			Element desnivel_positivo = doc1.createElement("desnivel_positivo");
			Text desnivel_positivoText = doc1.createTextNode(""
					+ desnivelTotalPositivo);
			desnivel_positivo.appendChild(desnivel_positivoText);
			rootElement.appendChild(desnivel_positivo);

			Element desnivel_negativo = doc1.createElement("desnivel_negativo");
			Text desnivel_negativoText = doc1.createTextNode(""
					+ desnivelTotalNegativo);
			desnivel_negativo.appendChild(desnivel_negativoText);
			rootElement.appendChild(desnivel_negativo);

			Element desnivel_acumulado = doc1
					.createElement("desnivel_acumulado");
			Text desnivel_acumuladoText = doc1.createTextNode(""
					+ (desnivelTotalPositivo - desnivelTotalNegativo));
			desnivel_acumulado.appendChild(desnivel_acumuladoText);
			rootElement.appendChild(desnivel_acumulado);

			Element tiempo = doc1.createElement("tiempo");
			Text tiempoText = doc1.createTextNode(""
					+ (tiempoFinal - tiempoInicio));
			tiempo.appendChild(tiempoText);
			rootElement.appendChild(tiempo);

			// escribimos el contenido en un fichero xml
			// =========================================
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer t = tf.newTransformer();
			// añadimos indentacion al xml
			t.setOutputProperty(OutputKeys.INDENT, "yes");
			// creamos el fichero
			DOMSource dSource = new DOMSource(doc1);
			StreamResult sResult = new StreamResult(new File("resumenesRutas/" + resumenRutaFileName));
			t.transform(dSource, sResult);
			
//			System.out.println("\ntodo ok !");
			return true;

		} catch (ParserConfigurationException pce) {
//			TUI.printError("fichero xml mal formado");
//			TUI.appControl();
//			GPX.successOnSubirRuta = false;
		} catch (TransformerException tfe) {
//			TUI.printError("fichero xml mal formado");
//			TUI.appControl();
//			GPX.successOnSubirRuta = false;
		} catch (SAXException e) {
//			TUI.printError("fichero xml mal formado");
//			TUI.appControl();
//			GPX.successOnSubirRuta = false;
		} 
		catch (IOException e) {
//			TUI.printError("el fichero no existe");
//			GPX.successOnSubirRuta = false;
//			GPX.almacenaAccionEnDb4o(user, 1, GPX.successOnSubirRuta);
		}
		return false;
	}
	
	
	/**
	 * Dado una lista de resúmenes, genera un documento xml con todos
	 * los resúmenes. El documento es temporal y será borrado una vez
	 * se genere el documento html a partir del xml generado.
	 * 
	 * @param resumenes los resumenes de las rutas
	 * @param ficheroXmlSalida nombre del fichero de salida
	 * @return true si se tiene éxito al generar el documento, false en otro caso
	 */
	public static boolean generaTodosResumen(String user, ArrayList<Resumen> resumenes, String ficheroXmlSalida){

		try {
			//iniciamos una instancia de DocumentBuilder
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			
			// Añadimos elemento raiz
			Element rootElement = doc.createElement("resumenes");
			doc.appendChild(rootElement);
			
			// añadimos un atributo al nodo resumenes
			Attr attr = doc.createAttribute("usuario");
			attr.setValue(user);
			rootElement.setAttributeNode(attr);
			
			//por cada Resumen en resumenes añadimos un resumen al fichero
			for(Resumen r : resumenes){	
				
				//añadimos el nodo resumen
				Element resumen = doc.createElement("resumen");
				rootElement.appendChild(resumen);
				
				//añadimos el nodo usuario
				Element usuario = doc.createElement("usuario");
				usuario.appendChild(doc.createTextNode(r.getNombreUsuario()));
				resumen.appendChild(usuario);

				//añadimos el nodo ruta_nombre
				Element nombreRuta = doc.createElement("ruta_nombre");
				nombreRuta.appendChild(doc.createTextNode(r.getNombre()));
				resumen.appendChild(nombreRuta);
				
				//añadimos el nodo descripcion
				Element descripcion = doc.createElement("descripcion");
				descripcion.appendChild(doc.createTextNode(r.getDescripcion()));
				resumen.appendChild(descripcion);
				
				//añadimos el nodo distancia
				Element distancia = doc.createElement("distancia");
				distancia.appendChild(doc.createTextNode("" + r.getDistancia()));
				resumen.appendChild(distancia);
				
				//añdimos el nodo desnivel
				Element desnivel = doc.createElement("desnivel");
				desnivel.appendChild(doc.createTextNode("" + r.getDesnivel()));
				resumen.appendChild(desnivel);

			}

			// escribimos el contenido en un fichero
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			//XML indentado
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File("resumenesRutas/" + ficheroXmlSalida));
	 
			transformer.transform(source, result);
			
		  } catch (ParserConfigurationException pce) {
			return false;
		  } catch (TransformerException tfe) {
			return false;
		  }
		return true;
	}
}
