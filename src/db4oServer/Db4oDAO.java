package db4oServer;

import java.util.ArrayList;

import postgresDAO.PostgresDAO;

import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.query.Predicate;

import dbServersMain.TUI;


/**
 * Data Access Object para realizar consultas sobre la BD db4o
 * 
 * @author Emilio Amado 2015
 *
 */
public class Db4oDAO {
	
	
	/**
	 * recuperamos todas los usuarios.
	 * 
	 * @param db db4o que contiene los usuarios
	 */
	public static void recuperaTodosUsuarios(ObjectContainer db) {
		
		ArrayList<Usuario> usuarios = new ArrayList<Usuario>();

		// almacenamos los usuarios en un Objectset
		ObjectSet<Usuario> result = db.query(new Predicate<Usuario>() {

			@Override
			public boolean match(Usuario aula) {
				return true;
			}
		});

		while (result.hasNext()) {
			Usuario u = result.next();
			usuarios.add(u);
		}
		
		if(usuarios.size() == 0){
			TUI.printError("aún no hay usuarios registrados");
			return;
		}
		
		System.out.println("\nAcciones :\n");
		for(Usuario u : usuarios){
			System.out.println("\t" + u);
		}
//		System.out.println(usuarios.size());
		
		System.out.println();
	}
	
	
	/**
	 * El usuario cuyas rutas han sido mas consultadas
	 * 
	 * @param db db4o que contiene los usuarios
	 */
	public static void recuperaUsuarioMaxConsultas(ObjectContainer db){
		ObjectSet<ConsultaRuta> result = db.query(new Predicate<ConsultaRuta>() {

			@Override
			public boolean match(ConsultaRuta consultaRuta) {
				return true;
			}
		});
		
		ArrayList<String> nombresUsuarios = PostgresDAO.countusuarios();
		int[] arrayCuentaConsultas = new int[nombresUsuarios.size()];
		ArrayList<ConsultaRuta> consultas = new ArrayList<ConsultaRuta>();
		
		while(result.hasNext()){
			ConsultaRuta consulta = result.next();
			consultas.add(consulta);
//			System.out.println(consulta);
		}
		
		int cuenta =0;
		for(int i=0;i<nombresUsuarios.size();i++ ){
			cuenta =0;
			String nombreI = nombresUsuarios.get(i);
			for(int j=0;j<consultas.size();j++){
				String nombreJ = consultas.get(j).getNombreUsuario();
				if(nombreI.equals(nombreJ)){
					cuenta++;
				}
			}
			arrayCuentaConsultas[i] = cuenta;
		}
		
		//buscamos el usuario con mayor valor
		Integer max = Integer.MIN_VALUE;
		String nombreRuta="";
		int posicionMaxDistancia = 0;
		//calculamos la distancia mas larga
		for(int i =0;i<arrayCuentaConsultas.length;i++){
			int distancia = arrayCuentaConsultas[i];
			if(distancia > max){
				max = distancia;
				posicionMaxDistancia = i;
			}
		}
		if(nombresUsuarios.size() == 0){
			TUI.printError("aún no hay usuarios registrados");
		}
		else{
			System.out.println("\n Usuario cuyas rutas se han consultado más :\n");
			System.out.println("\t"+nombresUsuarios.get(posicionMaxDistancia) +" : " + max + " consultas ");
		}
		return;
	}
	
	
	/**
	 * El usuario que más acciones ha realizado.
	 * 
	 * @param db db4o que contiene los usuarios
	 */
	public static void recuperaUsuarioConMasAcciones(ObjectContainer db){
		
		try{
		ArrayList<Usuario> usuarios = new ArrayList<Usuario>();
		
		System.out.print("\nUsuario con más acciones : \n");

		// almacenamos los usuarios en un Objectset
		ObjectSet<Usuario> result = db.query(new Predicate<Usuario>() {

			@Override
			public boolean match(Usuario aula) {
				return true;
			}
		});

		// introduzco los usuarios en un array
		while (result.hasNext()) {
			usuarios.add(result.next());
		}
		
		//obtenemos el total de apariciones de cada usuario
		int[] arrayRepsUsuarios = new int[usuarios.size()];
		int repsUsuario = 0;
		for(int i=0;i<usuarios.size();i++){
			repsUsuario = 0;
			String nombreUsuarioI = usuarios.get(i).getNombre();
			for(int j=0;j<usuarios.size();j++){
				String nombreUsuarioJ = usuarios.get(j).getNombre();
				if(nombreUsuarioI.equals(nombreUsuarioJ)){
					repsUsuario ++;
				}
			}
			arrayRepsUsuarios[i] = repsUsuario;
		}
		
		//obtenemos el máximo valor del array y su posicion
		int max = Integer.MIN_VALUE;
		int maxValuePositon = 0;
		for(int i =0;i<arrayRepsUsuarios.length;i++){
			if(arrayRepsUsuarios[i] >= max){
				max = arrayRepsUsuarios[i];
				maxValuePositon = i;
			}
		}
		
		//obtenemos el usuario mas repetido
		String u = usuarios.get(maxValuePositon).getNombre();
		System.out.println(u + "\n");
		
//		for(int i=0;i<arrayRepsUsuarios.length;i++){
//			System.out.println(arrayRepsUsuarios[i] + " - " + usuarios.get(i));
//		}
		
		}
		catch(IndexOutOfBoundsException e){
			TUI.printError("aún no hay usuarios registrados");
			TUI.appControl();
		}
		
	}	
	
	
	/**
	 * recuperamos el numero de acciones del usuario visitante.
	 * 
	 * @param db db4o que contiene los usuarios
	 */
	public static void recuperaTotalAccionesVisitante(ObjectContainer db) {

		System.out.print("\nTotal de acciones : ");

		// almacenamos las usuarios en un Objectset
		ObjectSet<Usuario> result = db.query(new Predicate<Usuario>() {

			@Override
			public boolean match(Usuario aula) {
				return true;
			}
		});
		int totalAcciones = 0;
		// imprimimos los resultados
		while (result.hasNext()) {
			Usuario u = result.next();
			if(u.getNombre().equals("visitante")){
				totalAcciones ++;
			}
		}
		System.out.print(totalAcciones + "\n\n");
	}
	
	
	/**
	 * Recuperamos el usuario que más rutas ha subido.
	 * 
	 * @param db db4o que contiene los usuarios
	 */
	public static void usuarioMasRutasSubidas(ObjectContainer db){
		try{
			ArrayList<Usuario> usuarios = new ArrayList<Usuario>();
			
			System.out.print("\nUsuario que más rutas ha subido : \n");
	
			// almacenamos los usuarios en un Objectset
			ObjectSet<Usuario> result = db.query(new Predicate<Usuario>() {
	
				@Override
				public boolean match(Usuario aula) {
					return true;
				}
			});
	
			// introduzco los usuarios en un array
			while (result.hasNext()) {
				usuarios.add(result.next());
			}
			
			//busco las subidas de los usuarios
			int[] subidasOkUsuarios = new int[usuarios.size()];
			
			int subidas = 0;
			for(int i=0;i<usuarios.size();i++) {
				subidas =0;
				Usuario usuarioI = usuarios.get(i);
				String nombreI = usuarioI.getNombre();
				String accionI = usuarioI.getAccionRealizada();
				boolean exitoI = usuarioI.isExito();
				for(int j=0;j<usuarios.size();j++){
					Usuario usuarioJ = usuarios.get(j);
					String nombreJ = usuarioJ.getNombre();
					String accionJ = usuarioJ.getAccionRealizada();
					boolean exitoJ = usuarioJ.isExito();
					if( (nombreI.equals(nombreJ)) &&
						(accionI.equals(accionJ)) &&
						(exitoI == exitoJ)){
						subidas ++;
					}
				}
				subidasOkUsuarios[i] = subidas;
			}	
			
			//obtenemos el máximo valor del array y su posicion
			int max = Integer.MIN_VALUE;
			int maxValuePositon = 0;
			for(int i =0;i<subidasOkUsuarios.length;i++){
				if(subidasOkUsuarios[i] >= max){
					max = subidasOkUsuarios[i];
					maxValuePositon = i;
				}
			}
			
			//obtenemos el usuario con mas rutas subidas
			String u = usuarios.get(maxValuePositon).getNombre();
			System.out.println(u + " : "+ max +" rutas\n");
		}
		catch(IndexOutOfBoundsException e){
			TUI.printError("aún no hay usuarios registrados");
			TUI.appControl();
		}
	}
	
	
	/**
	 * Elimina los objetos contenidos en la DB
	 * Objetos : Usuario y ConsultaRuta
	 * 
	 * @param db db4o que contiene los usuarios
	 * @return void
	 */
	public static void deleteDb4o(ObjectContainer db){
		
		ArrayList<Usuario> usuarios = new ArrayList<Usuario>();

		// almacenamos los usuarios en un Objectset
		ObjectSet<Usuario> result = db.query(new Predicate<Usuario>() {

			@Override
			public boolean match(Usuario usuario) {
				return true;
			}
		});

		// introduzco los usuarios en un array
		while (result.hasNext()) {
			usuarios.add(result.next());
		}
		
		for(Usuario u : usuarios){
			db.delete(u);
			db.commit();
		}
		
		ArrayList<ConsultaRuta> consultasRutas = new ArrayList<ConsultaRuta>();

		// almacenamos los usuarios en un Objectset
		ObjectSet<ConsultaRuta> result2 = db.query(new Predicate<ConsultaRuta>() {

			@Override
			public boolean match(ConsultaRuta consulta) {
				return true;
			}
		});

		// introduzco las consultas en un array
		while (result2.hasNext()) {
			consultasRutas.add(result2.next());
		}
		
		for(ConsultaRuta c : consultasRutas){
			db.delete(c);
			db.commit();
		}
		
	}

}
