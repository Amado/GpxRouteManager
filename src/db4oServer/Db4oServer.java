package db4oServer;


import com.db4o.ObjectServer;
import com.db4o.cs.Db4oClientServer;


/**
 * Crea un nuevo servidor 'Db4oClientServer', además de especificar los
 * usuarios que pueden conectarse al servidor.
 * 
 * @author Emilio Amado 2015
 * @see Db4oClientServer
 *
 */
public class Db4oServer implements Runnable {

	public static boolean stopDb4oServer = false;

	@Override
	public void run() {
		synchronized (this) {
			ObjectServer server = Db4oClientServer.openServer(
					Db4oClientServer.newServerConfiguration(),
					"db4oFiles/d4oServer.yap", // el nombre del fichero del servidor
					8732); // el puerto para el servidor

			Thread.currentThread().setName(this.getClass().getName());

			// creamos usuarios y damos permisos de acceso
			server.grantAccess("user", // el nombre del usuario
							   "1234");// el password para el usuario
			try {
				while (!stopDb4oServer) {
//					System.out.println("SERVER:[" + System.currentTimeMillis() + "] Server's running... ");
//					this.wait(6000);
				}
				server.close();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				server.close();
			}
		}

	}
}
