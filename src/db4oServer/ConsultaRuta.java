package db4oServer;


/**
 * Modela la consulta realizada a una ruta.
 * 
 * @author Emilio Amado.
 *
 */
public class ConsultaRuta {
	
	String nombreRuta;
	String nombreUsuario;
	
	public ConsultaRuta(String nombreRuta, String nombreUsuario) {
		super();
		this.nombreRuta = nombreRuta;
		this.nombreUsuario = nombreUsuario;
	}

	public String getNombreRuta() {
		return nombreRuta;
	}

	public void setNombreRuta(String nombreRuta) {
		this.nombreRuta = nombreRuta;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	@Override
	public String toString() {
		return "ConsultaRuta [nombreRuta=" + nombreRuta + ", nombreUsuario="
				+ nombreUsuario + "]";
	}

}
