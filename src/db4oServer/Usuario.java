package db4oServer;

import java.util.Arrays;

public class Usuario {
	
	private String nombre;
	
	private static final String[] listaAccionesPosibles = 
		{"darseAlta","subirRuta","consultarMisRuta","verRuta","verTodasRutas"};
	
	private String accionRealizada;
	
	private boolean exito;
	
	public Usuario(String nombre, int actionCode, boolean exito){
		this.nombre = nombre;
		this.exito = exito;
		this.accionRealizada = listaAccionesPosibles[actionCode];
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getAccionRealizada() {
		return accionRealizada;
	}

	public void setAccionRealizada(String accionRealizada) {
		this.accionRealizada = accionRealizada;
	}

	public boolean isExito() {
		return exito;
	}

	public void setExito(boolean exito) {
		this.exito = exito;
	}

	public static String getListaAccionesPosibles() {
		return Arrays.toString(listaAccionesPosibles);
	}

	@Override
	public String toString() {
		return "Usuario [nombre=" + nombre + ", accionRealizada="
				+ accionRealizada + ", exito=" + exito + "]";
	}
}
