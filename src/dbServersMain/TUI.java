package dbServersMain;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 * Interfaz de texto TUI para la navegación y el uso de 'MisRutasApp'
 * Al iniciar la TUI se imprime en pantalla el menú principal con las
 * diferentes opciones de las que dispone la app para el usuario.
 * 
 * <p>Cada vez que el usuario comete un error, se informa por pantalla
 * del error y se vuelve a imprimir el menú principal de navegación.
 * 
 * @author Emilio Amado 2015
 *
 */
public class TUI implements Runnable{
	
	
	/**
	 * Principal método de la interfaz, toda la interfaz parte de aquí.
	 * Contiene objetos Scanner para permitir al usuario introducir
	 * datos en la interfaz, también contiene ayudas para el usuario.
	 * Una vez que el usuario introduce un comando, este es enviado
	 * al método de validación pertinente.
	 * 
	 * <p>Si hay algún error, este método se recarga y el usuario
	 * debe volver a empezar el proceso.
	 * 
	 * @return void
	 */
	public static void appControl(){
		
		Scanner scOpcion = new Scanner(System.in);
		Scanner scComandos = new Scanner(System.in);
		Scanner scConsulta = new Scanner(System.in);
		
		try{
			appMenu();
			System.out.print("> ");
			int opcionSeleccionada = scOpcion.nextInt();
			int consultaSeleccionada;
			String comandoApp;
			
			switch (opcionSeleccionada) {
			case 1:
				System.out.println("\n   1. Continuar.");
				System.out.println("   2. Volver atrás.\n");
				System.out.print("> ");
				opcionSeleccionada = scOpcion.nextInt();
				if(opcionSeleccionada == 1){
					System.out.println("\n Uso : 'GPX -add usuario password' + enter\n");
					System.out.print("> ");
					comandoApp = scComandos.nextLine();
					validaAltaUsuario(comandoApp);
				}
				else if(opcionSeleccionada == 2){
					
					appControl();
				}
				else{
					System.out.println("\n Opción desconocida\n");
					appControl();
				}
				break;
				
			case 2: 
				System.out.println("\n   1. Continuar.");
				System.out.println("   2. Volver atrás.\n");
				System.out.print("> ");
				opcionSeleccionada = scOpcion.nextInt();
				if(opcionSeleccionada == 1){
					System.out.println("\n Uso : 'GPX -user nombre -pass password -ruta fichero.gpx "
						     + "-image imagen1.jpg imagen2.jpg "
						     + "-video video1.mpg video2.mpg' + enter\n");
					System.out.println(" Es muy importante que : \n"
									 + "\t-> El nombre de la ruta NO contenga ESPACIOS.\n"
									 + "\t-> La RUTA al directorio del fichero ruta.gpx sea ABSOLUTA.\n"
									 + "\t-> Ejemplo : /tmp/mi_ruta_por_cataluña.gpx\n");
					System.out.print("> ");
					comandoApp = scComandos.nextLine();
					validaSubirRuta(comandoApp);
				}
				else if(opcionSeleccionada == 2){
					appControl();
				}
				else{
					System.out.println("\n Opción desconocida\n");
					appControl();
				}
				break;
				
			case 3:
				System.out.println("\n   1. Continuar.");
				System.out.println("   2. Volver atrás.\n");
				System.out.print("> ");
				opcionSeleccionada = scOpcion.nextInt();
				if(opcionSeleccionada == 1){
					System.out.println("\n Uso : 'GPX -user nombre -pass password -output nombre.html' + enter\n");
					System.out.println("\tGenera un fichero html con el nombre dado por el usuario.\n");
					System.out.print("> ");
					comandoApp = scComandos.nextLine();
					validaConsultaMisRutas(comandoApp);
				}
				else if(opcionSeleccionada == 2){
					appControl();
				}
				else{
					System.out.println("\n Opción desconocida\n");
					appControl();
				}
				break;
				
			case 4:
				System.out.println("\n   1. Continuar.");
				System.out.println("   2. Volver atrás.\n");
				System.out.print("> ");
				opcionSeleccionada = scOpcion.nextInt();
				if(opcionSeleccionada == 1){
					System.out.println("\n Uso : 'GPX -find id_ruta -output' + enter\n");
					System.out.println("\tGenera un fichero html con el nombre original del fichero de la ruta.");
					System.out.println("\n Uso : 'GPX -find id_ruta -output nombre.html+ enter\n");
					System.out.println("\tGenera un fichero html con el nombre dado por el usuario.\n");
					System.out.print("> ");
					comandoApp = scComandos.nextLine();
					validaVerRuta(comandoApp);
				}
				else if(opcionSeleccionada == 2){
					appControl();
				}
				else{
					System.out.println("\n Opción desconocida\n");
					appControl();
				}
				break;
				
			case 5:
				System.out.println("\n   1. Continuar.");
				System.out.println("   2. Volver atrás.\n");
				System.out.print("> ");
				opcionSeleccionada = scOpcion.nextInt();
				if(opcionSeleccionada == 1){
					System.out.println("\n Uso : 'GPX -output salida.html' + enter\n");
					System.out.print("> ");
					comandoApp = scComandos.nextLine();
					validaConsutaTodasRutas(comandoApp);
				}
				else if(opcionSeleccionada == 2){
					appControl();
				}
				else{
					System.out.println("\n Opción desconocida\n");
					appControl();
				}
				break;
				
			case 6:
				System.out.println("\n   1. Continuar.");
				System.out.println("   2. Volver atrás.\n");
				System.out.print("> ");
				opcionSeleccionada = scOpcion.nextInt();
				if(opcionSeleccionada == 1){
					System.out.println("\n Selecciona una consulta :\n");
					menuConsultas();
					System.out.print("> ");
					try{
						consultaSeleccionada = scConsulta.nextInt();
						GPX.peticionConsultaUsuario(consultaSeleccionada);
					}
					catch(InputMismatchException e){
						System.out.println("\n Opción desconocida\n");
						appControl();
					}
					
				}
				else if(opcionSeleccionada == 2){
					appControl();
				}
				else{
					System.out.println("\n Opción desconocida\n");
					appControl();
				}
				break;
				
			case 7:
				GPX.peticionEliminarDB();
				appControl();
				break;
				
			case 8:
				System.out.println("\nAdios!\n");
				Main.paraTodo();
				break;
				
			default:
				break;
			}
		}
		catch(InputMismatchException e){
			System.out.println("\n Opción desconocida\n");
			appControl();
		}
	}
	
	/**
	 * Imprime en la interfaz el menú de navegación con las opciones posibles.
	 * 
	 * @return void
	 */
	private static void appMenu(){
		System.out.println();
		System.out.println("                    ===============");
		System.out.println("                    | MisRutasApp |");
		System.out.println("                    ===============");
		System.out.println(" ========================================================");
		System.out.println(" |                                                      |");
		System.out.println(" | Selecciona una opción , escribe el número + enter :  |");
		System.out.println(" |                                                      |");
		System.out.println(" |   1. Dar de alta un usuario.                         |");
		System.out.println(" |   2. Subir una ruta.                                 |");
		System.out.println(" |   3. Consultar mis rutas.                            |");
		System.out.println(" |   4. Ver una ruta.                                   |");
		System.out.println(" |   5. Consultar todas las rutas de los usuarios.      |");
		System.out.println(" |   6. Realizar una consulta.                          |");
		System.out.println(" |   7. Eliminar DB.                                    |");
		System.out.println(" |   8. Salir del programa.                             |");
		System.out.println(" |                                                      |");
		System.out.println(" ========================================================\n");
	}

	/**
	 * Imprime en la interfaz el menu de consultas que
	 * el usuario puede realizar.
	 * 
	 * @return void
	 */
	private static void menuConsultas(){
		System.out.println("   1.  El usuario que más rutas ha subido.");
		System.out.println("   2.  El usuario que ha subido más Km de ruta.");
		System.out.println("   3.  El usuario que más acciones a realizado.");
		System.out.println("   4.  Cuantos acciones ha realizado el usuario visitante.");
		System.out.println("   5.  Relación de todas las acciones realizadas.");
		System.out.println("   6.  El usuario que haya subido rutas con el mayor desnivel.");
		System.out.println("   7.  De que usuario han sido las rutas más consultadas.");
		System.out.println("   8.  Cual es la ruta más corta?");
		System.out.println("   9.  Cual es la ruta más larga?");
		System.out.println("   10. Imprime una relación de todos los ficheros subidos.");
		System.out.println("   11. Imprime las id y los nombres de todas las rutas.");
		System.out.println("   12. Selecciona un fichero html y ábrelo con firefox.\n");
	}
	
	/**
	 * Valida el comando introducido por el usuario de la app para
	 * dar de alta a un nuevo usuario.
	 * 
	 * <p>Si el usuario de la app comete algún error de sintaxis,
	 * se informa por pantalla y se imprime una ayuda.
	 * 
	 * @param comando el comando a evaluar su sintaxis
	 */
	private static void validaAltaUsuario(String comando){
		
		String[] comandoArray = comando.split(" ");
		
		if(comandoArray.length != 4){
			printError("número de argumentos incorrecto");
			appControl();
			return;
		}
		else if( !comandoArray[0].equals("GPX") ){
			printError("el primer arg debe ser 'GPX'");
			appControl();
			return;
		}
		else if( !comandoArray[1].equals("-add") ){
			printError("el segundo arg debe ser '-add'");
			appControl();
			return;
		}
		else if( !(comandoArray[3].length() >= 6) ){
			printError("longitud mínima del pass es 6");
			appControl();
			return;
		}
		GPX.peticionAltaUsuario(comandoArray[2], comandoArray[3]);
	}	
	
	/**
	 * Valida el comando introducido por el usuario de la app para
	 * almacenar en el sistema una nueva ruta de usuario.
	 * 
	 * <p>Si el usuario de la app comete algún error de sintaxis,
	 * se informa por pantalla y se imprime una ayuda.
	 * 
	 * @param comando el comando a evaluar su sintaxis
	 */
	private static void validaSubirRuta(String comando){
		
		String[] comandoArray = comando.split(" ");
		
		if(comandoArray.length < 7){
			printError("número de argumentos incorrecto");
			appControl();
			return;
		}
		else if( !comandoArray[0].equals("GPX") ){
			printError("el primer arg debe ser 'GPX'");
			appControl();
		}
		else if( !comandoArray[1].equals("-user") ){
			printError("el segundo arg debe ser '-user'");
			appControl();
			return;
		}
		else if( !comandoArray[3].equals("-pass") ){
			printError("el cuarto arg debe ser '-pass'");
			GPX.almacenaAccionEnDb4o(comandoArray[2], 1, false);
			return;
		}
		else if( !comandoArray[5].equals("-ruta") ){
			printError("el sexto arg debe ser '-ruta'");
			GPX.almacenaAccionEnDb4o(comandoArray[2], 1, false);
			return;
		}
		else if( !(comandoArray[6].matches("^.*\\.gpx$"))){
			printError("extensiones de ruta permitidas:\n\t '.gpx'");
			GPX.almacenaAccionEnDb4o(comandoArray[2], 1, false);
			return;
		}
		
		List<String> imagenes = new ArrayList<String>();
		List<String> videos = new ArrayList<String>();
		
		for(int i=7;i<comandoArray.length;i++){
			String arg = comandoArray[i];
			if( !(arg.matches("^-image$|^-video$|^.*\\.jpg$|^.*\\.mpg$")) ){
				printError("en extensión de fichero o no haber introducido :"
						 + "\n\t el arg '-image' para almacenar imágenes o "
						 + "\n\t el arg '-video' para almacenar videos"
						 + "\n Extensiones permitidas : '.jpg' para imágenes, '.mpg' para videos");
				GPX.almacenaAccionEnDb4o(comandoArray[2], 1, false);
				return;
			}
			if( arg.matches("^.*\\.jpg$") ){
				imagenes.add(arg);
			}
			else if( arg.matches("^.*\\.mpg$") ){
				videos.add(arg);
			}
		}
		
//		System.out.println("\nimagenes");
//		for(String s : imagenes){
//			System.out.println(s);
//		}
//		System.out.println("\nvideos");
//		for(String s : videos){
//			System.out.println(s);
//		}
//		System.out.println("array longitud : " + comandoArray.length);
		GPX.peticionSubirRuta(comandoArray[2], comandoArray[4], comandoArray[6], imagenes, videos);
	}
	
	/**
	 * Valida el comando introducido por el usuario de la app para
	 * que un usuraio conslte sus rutas.
	 * 
	 * <p>Si el usuario de la app comete algún error de sintaxis,
	 * se informa por pantalla y se imprime una ayuda.
	 * 
	 * @param comando el comando a evaluar su sintaxis
	 */
	private static void validaConsultaMisRutas(String comando){
		
		String[] comandoArray = comando.split(" ");
		
		if(comandoArray.length != 7){
			printError("número de argumentos incorrecto");
			appControl();
			return;
		}
		else if( !comandoArray[0].equals("GPX") ){
			printError("el primer arg debe ser 'GPX'");
			appControl();
			return;
		}
		else if( !comandoArray[1].equals("-user") ){
			printError("el segundo arg debe ser '-user'");
			appControl();
			return;
		}
		else if( !comandoArray[3].equals("-pass") ){
			printError("el cuarto arg debe ser '-pass'");
			GPX.almacenaAccionEnDb4o(comandoArray[2], 2, false);
			return;
		}
		else if( !comandoArray[5].equals("-output") ){
			printError("el sexto arg debe ser '-output'");
			GPX.almacenaAccionEnDb4o(comandoArray[2], 2, false);
			return;
		}		
		else if( !comandoArray[6].matches("^.*\\.html") ){
			printError("incluye la extensión del fichero de salida como .html");
			GPX.almacenaAccionEnDb4o(comandoArray[2], 2, false);
			return;
		}
		
		GPX.peticionConsultaMisRutas(comandoArray[2], comandoArray[4], comandoArray[6]);
	}
	
	/**
	 * Valida el comando introducido por el usuario de la app para
	 * que cualquier usuario pueda consultar una ruta.
	 * 
	 * <p>Si el usuario de la app comete algún error de sintaxis,
	 * se informa por pantalla y se imprime una ayuda.
	 * 
	 * @param comando el comando a evaluar su sintaxis
	 */
	private static void validaVerRuta(String comando){
		
		String[] comandoArray = comando.split(" ");
		
		if( (comandoArray.length) != 5 && (comandoArray.length != 4) ){
			printError("número de argumentos incorrecto");
			GPX.almacenaAccionEnDb4o("visitante", 3, false);
			return;
		}
		else if( !comandoArray[0].equals("GPX") ){
			printError("el primer arg debe ser 'GPX'");
			GPX.almacenaAccionEnDb4o("visitante", 3, false);
			return;
		}
		else if( !comandoArray[1].equals("-find") ){
			printError("el segundo arg debe ser '-find'");
			GPX.almacenaAccionEnDb4o("visitante", 3, false);
			return;
		}
		else if( !comandoArray[3].equals("-output") ){
			printError("el cuarto arg debe ser -output");
			GPX.almacenaAccionEnDb4o("visitante", 3, false);
			return;
		}

		if(comandoArray.length == 4){
			GPX.peticionVerRuta(comandoArray[2], null);
			return;
		}
		else{
			if( !comandoArray[4].matches("^.*\\.html") ){
				printError("incluye la extensión del fichero de salida como .html");
				GPX.almacenaAccionEnDb4o("visitante", 3, false);
				return;
			}
			else{
				GPX.peticionVerRuta(comandoArray[2], comandoArray[4]);
			}
		}
	}
	
	/**
	 * Valida el comando introducido por el usuario de la app para
	 * que cualquier usuario consulte todas las rutas almacenadas
	 * en el sistema por todos los usuarios.
	 * 
	 * <p>Si el usuario de la app comete algún error de sintaxis,
	 * se informa por pantalla y se imprime una ayuda.
	 * 
	 * @param comando el comando a evaluar su sintaxis
	 */
	private static void validaConsutaTodasRutas(String comando){
		
		String[] comandoArray = comando.split(" ");
		
		if(comandoArray.length != 3){
			printError("número de argumentos incorrecto");
			GPX.almacenaAccionEnDb4o("visitante", 4, false);
			return;
		}
		else if( !comandoArray[0].equals("GPX") ){
			printError("el primer arg debe ser 'GPX'");
			GPX.almacenaAccionEnDb4o("visitante", 4, false);
			return;
		}
		else if( !comandoArray[1].equals("-output") ){
			printError("el segundo arg debe ser -output");
			GPX.almacenaAccionEnDb4o("visitante", 4, false);
			return;
		}
		else{
			if( !comandoArray[2].matches("^.*\\.html") ){
				printError("incluye la extensión del fichero de salida como .html");
				GPX.almacenaAccionEnDb4o("visitante", 4, false);
				return;
			}
			else{
				GPX.peticionConsultaTodasRutas(comandoArray[2]);
			}
		}
	}
	
	/**
	 * Mensaje de error del sistema producido por el usuario de la app.
	 * 
	 * @param error
	 */
	public static void printError(String error){
		System.out.println("\n Error, " + error + ".");
		System.out.println(" Por favor, vuelve a empezar.\n");
	}

	@Override
	public void run() {
		appControl();
	}

}
