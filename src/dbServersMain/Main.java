package dbServersMain;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import postgresDAO.PostgresDAO;
import baseXServer.BasexDAO;
import baseXServer.MyBaseXServer;

import com.db4o.ObjectContainer;

import db4oServer.Db4oServer;

/**
 * Inicializa  los módulos necesarios para la ejecución del programa.
 * Inicializa el cliente de postgresQl, el servidor de BaseX,
 * el servidor de Db4O, y la interfaz de texto para el usuario.
 * 
 * @author Emilio Amado 2015
 *
 */
public class Main {
	
	private static ObjectContainer db4oDBFile;
	private static Connection conPostgres;
	private static Thread db4oServerThread;
	private static Thread gpxClientThread;
	private static Thread basexServerThread;
	private static Thread basexClientThread;
	private static Thread tuiThread;
	
	
	public static void main(String[] args) {
		
		//inicializamos la Db4o 
		inicializaDb4o();

		//inicializamos postgresDAO
		inicializaPostgresClient();
		
		//inicializamos baseX
		inicializaBaseX();
		
//		//inicializamos la TUI
//		TUI.appControl();
		
		//creamos los directorios necesarios para los ficheros html 
		//que la app vaya a crear
		File file = new File(System.getProperty("user.home"), "MisRutasAppHtmlFiles");
		if(!file.exists()){
			file.mkdirs();
		}
	}
	
	
	/**
	 * Inicializa los servicios necesarios para la base de datos 'db4o'
	 * 
	 * @return void
	 */
	private static void inicializaDb4o(){
		//creamos un nuevo fichero para almacenar los objetos de la db4o
		
//		DateFormat dateFormat = new SimpleDateFormat("_yyyy-MM-dd_HH-mm-ss");
//		Date date = new Date();
//		
//		String currentDate = dateFormat.format(date);
		
//		db4oDBFile = Db4oEmbedded.openFile("db4oFiles/db4oDB" /*+ currentDate*/);
		
		//instanciamos nuestro servidor
		Db4oServer db4oServer = new Db4oServer();

		//instanciamos el cliente GPX
		GPX gpx = new GPX();
		
		//inicializamos la Db4o en un nuevo Thread para no bloquear la app
		db4oServerThread = new Thread(db4oServer,"db4oServer");
		gpxClientThread = new Thread(gpx,"gpxClient");
		
		//gestionamos la ejecucion de los hilos
		ScheduledExecutorService ses = Executors.newScheduledThreadPool(3);
		ses.schedule(db4oServerThread, 0, TimeUnit.SECONDS);//ejecutamos el servidor inmediatamente
		ses.schedule(gpxClientThread, 3, TimeUnit.SECONDS);//ejecutamos el cliente gpx
	}
	
	
	/**
	 * Inicializa el cliente para nuestro servidor postgresQl. Recordemos que la instalación
	 * de postgresQl en la máquina, lleva consigo un servidor, por lo que sólo necesitamos
	 * los clientes o el cliente que se vaya a conectar a postgresQl.
	 * 
	 * @return void
	 */
	private static void inicializaPostgresClient(){
		try{
			//Cargamos el driver(librería)
			Class.forName("org.postgresql.Driver");
			System.out.println( " Driver postgresQl cargado ");
			
			//establecemos la conexión a la BD
			conPostgres = DriverManager.getConnection(
				"jdbc:postgresql://localhost:5432/gpxdb","test","emilio1980");
			
			PostgresDAO.setPostgresConnection(conPostgres);
			
			System.out.println(" Conexión a postgresQl realizada\n");
			
		}catch(ClassNotFoundException e){
			e.printStackTrace();
		}catch(SQLException e){
			e.printStackTrace();
		}	
	}
	
	
	/**
	 * Inicializa el servidor de BaseX y el cliente. Además, dado que todos los
	 * servidores y clientes para la app, quedan inicializados, en la última
	 * orden inicializamos la TUI (Text User Interface)
	 * 
	 * @return void
	 */
	private static void inicializaBaseX(){

		//instanciamos nuestro servidor y cliente
		MyBaseXServer myBasexServer = new MyBaseXServer();
		BasexDAO basexClientSession = new BasexDAO();
		TUI tui = new TUI();
		
		//lo inicializamos en un nuevo Thread
		basexServerThread = new Thread(myBasexServer,"basexServer");
		basexClientThread = new Thread(basexClientSession,"basexClient");
		tuiThread = new Thread(tui,"tui");
		
		//gestionamos la ejecucion de los hilos
		ScheduledExecutorService ses = Executors.newScheduledThreadPool(3);
		ses.schedule(basexServerThread, 0, TimeUnit.SECONDS);//ejecutamos el servidor inmediatamente
		ses.schedule(basexClientThread, 3, TimeUnit.SECONDS);//ejecutamos el cliente basex
		ses.schedule(tui, 1, TimeUnit.SECONDS);//ejecutamos la TUI
	}
	
	/**
	 * Detiene por completo la app, incluidos todos los Threads, servers y clientes.
	 * 
	 * @return void
	 */
	public static void paraTodo(){
		try {
			conPostgres.close();
			gpxClientThread.interrupt();
			basexClientThread.interrupt();
			tuiThread.interrupt();
			GPX.stopDb4oClient = true;
			Db4oServer.stopDb4oServer = true;
//			db4oDBFile.close();
			MyBaseXServer.stopBasexServer = true;
			db4oServerThread.interrupt();
			basexServerThread.interrupt();
			System.exit(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
