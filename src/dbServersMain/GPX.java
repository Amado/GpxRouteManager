package dbServersMain;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import postgresDAO.LeeResumenDOM;
import postgresDAO.PostgresDAO;
import postgresDAO.Resumen;
import resumenRuta.GeneraResumenRuta;
import xsltTransformer.XsltRutas;
import baseXServer.BasexDAO;

import com.db4o.ObjectContainer;
import com.db4o.cs.Db4oClientServer;

import db4oServer.ConsultaRuta;
import db4oServer.Db4oDAO;
import db4oServer.Usuario;

/**
 * Se encarga de procesar las peticiones a realizar sobre los servidores
 * ya sea el servidor postgresQl, el de BaseX o el de Db4o, además de usar
 * los diferentes DAO (Data Access Object) para cada Servidor.
 * 
 * @author Emilio Amado 2015
 *
 */
public class GPX implements Runnable{
	
	private static boolean isNewUserNameOk;
	private static boolean isNewUserPassOk;
	private static boolean isUserNameOk;
	private static boolean isUserPassOk;
	private static boolean isRutaIdOk;
	
	
	public static boolean successOnAltaUsuario = false;
	public static boolean successOnSubirRuta = false;
	public static boolean successOnConsultaMisRutas = false;
	public static boolean successOnVerRuta = false;
	public static boolean successOnConsultaTodasRutas = false;
	
	public static boolean stopDb4oClient = false;
	private static ObjectContainer db4oClient = null;
	
	private static Scanner sc = new Scanner(System.in);
	
	/**
	 * Realiza una petición al sistema para dar de alta a un nuevo usuario.
	 * Se debe comprobar que el usuario no exista.
	 * 
	 * @param user nombre del nuevo usuario a almacenar en el sistema
	 * @param pass password del nuevo usuario a almacenar en el sistema
	 * 
	 * @return void
	 */
	public static void peticionAltaUsuario(String user, String pass){
		
		//almacenamos el nuevo usuario en postgresQl
		successOnAltaUsuario = PostgresDAO.insertIntoUsuarios(user, pass);

		//alamcenamos la acción en la BD de db4o
		almacenaAccionEnDb4o(user, 0, successOnAltaUsuario);
	}
	
	/**
	 * Almacena en el sistema una nueva ruta.
	 * 
	 * <p>Primero debemos validar que el usuario y la contraseña son los correctos. 
	 * Debemos procesar el fichero gpx y recuperar el nombre, descripción, calcular 
	 * el desnivel positivo y la distancia, generar un resumen en xml y almacenarlo 
	 * en la tabla rutas. En la tabla documentos almacenaremos los nombres de los 
	 * ficheros asociados a la ruta, y el resumen generado el nombre de los ficheros 
	 * será el nombre del usuario+id_ruta+id_documento. Una vez hecho esto se 
	 * deberán almacenar estos documentos en el servidor basex.
	 * 
	 * <p>En el servidor baseX también tendremos un xml que se llamará rutas, 
	 * estará compuesto por todas los resúmenes.
	 * 
	 * @param user nombre de usuario para el login al sistema
	 * @param pass password del usuario para el login
	 * @param ruta fichero .gpx a almacenar
	 * @param imagenes imágenes asociadas a la ruta que se almacena
	 * @param videos videos asociados a la ruta que se almacena
	 * @return void
	 */
	public static void peticionSubirRuta(String user, String pass, String ruta, List<String> imagenes, List<String> videos){
		//primero validamos el login del usuario
		successOnSubirRuta = PostgresDAO.validaLoginUsuario(user, pass);
		//si el login falla salimos
		if(!successOnSubirRuta){
			//alamcenamos la acción en la BD de db4o
			almacenaAccionEnDb4o(user, 1, successOnSubirRuta);
			return;
		}
		
		//generamos un resumen del fichero ruta.gpx
		String nombreRutaFIle = ruta;
		String[] arrayRutaFile = nombreRutaFIle.split("/");
		String nombreResumen = arrayRutaFile[arrayRutaFile.length-1] + "_RESUMEN.gpx";
		//si la ruta al fichero es erronea salimos
		successOnSubirRuta = GeneraResumenRuta.generaResumen(user, ruta, nombreResumen);
		if(!successOnSubirRuta){
			//alamcenamos la acción en la BD de db4o
			almacenaAccionEnDb4o(user, 1, successOnSubirRuta);
			return;
		}
		
		//obtenemos los datos del resumen generado
		LeeResumenDOM.LeeResumen(nombreResumen);
		String nombreRuta = LeeResumenDOM.getNombreRuta();
		String descripcionRuta = LeeResumenDOM.getDescripcionRuta();
		String desnivelPositivo = LeeResumenDOM.getDesnivelPositivoRuta();
		String distancia = LeeResumenDOM.getDistanciaTotalRuta();
		
		//añadimos el resumen a la BD baseX
		BasexDAO.addToBasex("resumenesRutas/"+nombreResumen);
		
		//borramos el fichero del resumen, ya no nos hace falta
		File fileToDelete = new File("resumenesRutas/"+nombreResumen);
		fileToDelete.delete();
		
		//===================================================
		// ATENCION aquí debemos subir todos los ficheros al servidor 
		// BaseX , si esto falla por que la ruta al directorio de algún 
		// fichero está mal especificada, o el fichero no existe
		// debemos salir del método.
		boolean isFilePathOk;
		BasexDAO.alamcenaFichero(ruta);
		
//		BasexDAO.printRutasContent();
		
		if(imagenes.size() > 0){
			for(String s : imagenes){
				isFilePathOk = BasexDAO.alamcenaFichero(s);
				if(!isFilePathOk){
					successOnSubirRuta = false;
					//alamcenamos la acción en la BD de db4o
					almacenaAccionEnDb4o(user, 1, successOnSubirRuta);
					return;
				}
			}
		}
		
		if(videos.size() > 0){
			for(String s : videos){
				isFilePathOk = BasexDAO.alamcenaFichero(s);
				if(!isFilePathOk){
					successOnSubirRuta = false;
					//alamcenamos la acción en la BD de db4o
					almacenaAccionEnDb4o(user, 1, successOnSubirRuta);
					return;
				}
			}
		}
		//===================================================
		
		//obtenemos el nombre de la ruta para almacenarlo con el mismo nombre
		String[] arrayFilePath = ruta.split("/");
		String rutaName = arrayFilePath[arrayFilePath.length-1];
		
		//hacemos la insercion en la tabla rutas
		PostgresDAO.insertIntoRutas(user, rutaName, descripcionRuta, desnivelPositivo,distancia);

		//hacemos la insercion de los documentos en la tabla documentos
		//insertamos el resumen de la ruta
		int docId = PostgresDAO.selectLastDocumentId()+1;
		int rutaId = PostgresDAO.selectIdRuta(rutaName);
		String nombreDocumento=user+"_"+rutaId+"_"+docId;
		PostgresDAO.insertIntoDocumentos(nombreDocumento, rutaId);
	
		//si tenemos imagenes que insertar
		if(imagenes.size() > 0){
			for(String s : imagenes){
				docId = PostgresDAO.selectLastDocumentId()+1;
				rutaId = PostgresDAO.selectIdRuta(rutaName);
				nombreDocumento=nombreDocumento=user+"_"+rutaId+"_"+docId;
				PostgresDAO.insertIntoDocumentos(nombreDocumento, rutaId);
			}
		}
		//si tenemos videos que insertar
		if(videos.size() > 0){
			for(String s : videos){
				docId = PostgresDAO.selectLastDocumentId()+1;
				rutaId = PostgresDAO.selectIdRuta(rutaName);
				nombreDocumento=nombreDocumento=user+"_"+rutaId+"_"+docId;
				PostgresDAO.insertIntoDocumentos(nombreDocumento, rutaId);
			}
		}
		//alamcenamos la acción en la BD de db4o
		almacenaAccionEnDb4o(user, 1, successOnSubirRuta);
	}
	
	/**
	 * Realiza una petición al sistema para que usuario consulte sus rutas.
	 * Se validan el usuario y el pass para generar una salida html con 
	 * el resumen de todas las rutas subidas por el usuario.
	 * 
	 * @param user nombre de usuario para el login al sistema
	 * @param pass password del usuario para el login
	 * @return void
	 */
	public static void peticionConsultaMisRutas(String user, String pass, String ficheroHtmlSalida){

		//primero validamos el login del usuario
		successOnConsultaMisRutas = PostgresDAO.validaLoginUsuario(user, pass);
		
		//si el login falla salimos
		if(!successOnConsultaMisRutas){
			almacenaAccionEnDb4o(user, 2, successOnConsultaMisRutas);
			return;
		}
		
		
		//obtenemos todos los resumenes del usuario
		ArrayList<Resumen> resumenes = PostgresDAO.selectAllRutasFromUsuario(user);
		//si la lista está vacía salimos con error
		if(resumenes.size() == 0){
			TUI.printError("el usuario no ha subido ninguna ruta");
			successOnConsultaMisRutas = false;
			almacenaAccionEnDb4o(user, 2, successOnConsultaMisRutas);
			return;
		}
		
		//================================================
		//si la lista no está vacía :
		//	1º imprimimos los resúmenes por consola
		//	2º generamos un fichero xml temporal
		//	3º generamos el fichero html y borramos el xml
		//================================================
		
		//imprimimos los resumenes
		for(Resumen r : resumenes){
			System.out.println(r + "\n");
		}
		
		//generamos el fichero xml
		String nombreXmlSalida = "todosResumenes.xml";
		successOnConsultaMisRutas = GeneraResumenRuta.generaTodosResumen(user, resumenes, nombreXmlSalida);
		//si hay algún error salimos
		if(!successOnConsultaMisRutas){
			TUI.printError("al generar el documento con todos los resumenes");
			almacenaAccionEnDb4o(user, 2, successOnConsultaMisRutas);
			return;
		}
		
		//generamos el html
		XsltRutas.xsltTodasRutasUsuario(nombreXmlSalida, ficheroHtmlSalida);
		
		//borramos el fichero de todos los resumenes, ya no nos hace falta
		File fileToDelete = new File("resumenesRutas/"+nombreXmlSalida);
		fileToDelete.delete();

		//alamcenamos la acción en la BD de db4o
		almacenaAccionEnDb4o(user, 2, successOnConsultaMisRutas);
	}
	
	/**
	 * Realiza una petición al sistema para que cualquier usuario pueda
	 * consultar una ruta con todos sus puntos. 
	 * Dado el identificador de una ruta generar una salida en html
	 * con toda la ruta y sus puntos e imprime el resumen en pantalla.
	 * 
	 * @param rutaId la id de la ruta a consultar
	 * @return void
	 */
	public static void peticionVerRuta(String rutaId, String html){
		
		// con la id de la ruta obtenemos de PostgresDAO toda la info de la ruta
		Resumen resumen = PostgresDAO.selectAllFromRutas(Integer.parseInt(rutaId));
		if(resumen == null){
			successOnVerRuta = false;
			TUI.printError("la ruta no existe");
			almacenaAccionEnDb4o("visitante", 3, successOnVerRuta);
			return;
		}
		//a partir de la info generamos un fichero html con la plantilla XSD
		//recuperamos el ficuero raw de baseX
		BasexDAO.recuperaFichero(resumen.getNombre());
		
		//generamos el nombre para el html
		String[] arrayNombreResumen = resumen.getNombre().split("\\.");
		String nombreHtml="";
		for(int i=0;i<arrayNombreResumen.length-1;i++){
			nombreHtml = nombreHtml + arrayNombreResumen[i];
		}
		nombreHtml = nombreHtml +".html";
		
		if(html != null){
			nombreHtml = html;
		}
		
		//generamos el html con la ruta completa
		XsltRutas.xsltRutaCompleta(resumen.getNombre(), nombreHtml);
		
		//borramos el fichero raw con la ruta, ya no nos hace falta
		File fileToDelete = new File("basexFiles/"+resumen.getNombre());
		fileToDelete.delete();
		
		//imprimimos por pantalla el resumen
		System.out.println(resumen.toString());
		System.out.println("\n Fichero .html generado en la carpeta html");
		
		//almacenamos la acción en la db4o ConsultaRuta
		String nombreRuta=resumen.getNombre();
		String nombreUsuario =resumen.getNombreUsuario();
		ConsultaRuta consulta = new ConsultaRuta(nombreRuta,nombreUsuario);
		db4oClient.store(consulta);
		db4oClient.commit();
		
		successOnVerRuta = true;
		//alamcenamos la acción en la BD de db4o Usuario
		almacenaAccionEnDb4o("visitante", 3, successOnVerRuta);
	}
	
	/**
	 * Realiza una petición al sistema para que cualuier usuario pueda consultar
	 * todas las rutas almacenadas en el sistema.
	 * Se recuperarán todas las rutas que hayan subido los usuarios mostrando 
	 * el nombre, la descripción, el desnivel positivo y la distancia. Todos estos 
	 * datos los recuperaremos del servidor postgres o del fichero resumenes. 
	 * La salida será en formato html.
	 * 
	 * @return void
	 */
	public static void peticionConsultaTodasRutas(String ficheroHtmlSalida){
		
		//obtenemos todos los resumenes 
		ArrayList<Resumen> resumenes = PostgresDAO.selectAllrutas();
		//si la lista está vacía salimos con error
		if(resumenes.size() == 0){
			TUI.printError("aún no hay rutas almacenadas");
			successOnConsultaTodasRutas = false;
			almacenaAccionEnDb4o("visitante", 4, successOnConsultaTodasRutas);
			return;
		}
		
		//================================================
		//si la lista no está vacía :
		//	1º imprimimos los resúmenes por consola
		//	2º generamos un fichero xml temporal
		//	3º generamos el fichero html y borramos el xml
		//================================================
		
		//imprimimos los resumenes
		for(Resumen r : resumenes){
			System.out.println(r + "\n");
		}
		
		//generamos el fichero xml
		String nombreXmlSalida = "todosResumenes.xml";
		successOnConsultaTodasRutas = GeneraResumenRuta.generaTodosResumen("todos", resumenes, nombreXmlSalida);
		//si hay algún error salimos
		if(!successOnConsultaTodasRutas){
			TUI.printError("al generar el documento con todos los resumenes");
			almacenaAccionEnDb4o("visitante", 4, successOnConsultaTodasRutas);
			return;
		}
		
		//generamos el html
		XsltRutas.xsltTodasRutas(nombreXmlSalida, ficheroHtmlSalida);
		
		//borramos el fichero de todos los resumenes, ya no nos hace falta
		File fileToDelete = new File("resumenesRutas/"+nombreXmlSalida);
		fileToDelete.delete();

		//alamcenamos la acción en la BD de db4o
		almacenaAccionEnDb4o("visitante", 4, successOnConsultaTodasRutas);
	}
	
	/**
	 * Realiza una petición al sistema para que el usuario pueda obtener cierta
	 * información dependiendo de la opción seleccionada.
	 * 
	 * <p>Opciones posibles :
	 * 
	 * 		1 El usuario que más rutas ha subido. 
	 *		2 El usuario que ha subido más Km de ruta.
 	 *		3 El usuario que más acciones ha realizado.
	 *	 	4 Cuantos acciones ha realizado el usuario visitante.
	 *		5 Relación de todas las acciones realizadas"
	 *		6 El usuario que haya subido rutas con el mayor desnivel.
	 *		7 De que usuario han sido las rutas más consultadas.
	 *		8 Cual es la ruta más corta?
	 *		9 Cual es la ruta más larga?
	 * 
	 * @param opcion la opción seleccionada por el usuario
	 * @return void
	 */
	public static void peticionConsultaUsuario(int opcion){
		switch (opcion) {
		case 1:
			Db4oDAO.usuarioMasRutasSubidas(db4oClient);
			//recargamos el menu principal
			TUI.appControl();
			break;
			
		case 2:
			PostgresDAO.selectUsuarioConMasKM();
			//recargamos el menu principal
			TUI.appControl();
			break;

		case 3:
			Db4oDAO.recuperaUsuarioConMasAcciones(db4oClient);
			//recargamos el menu principal
			TUI.appControl();
			break;

		case 4:
			Db4oDAO.recuperaTotalAccionesVisitante(db4oClient);
			//recargamos el menu principal
			TUI.appControl();
			break;

		case 5:
			Db4oDAO.recuperaTodosUsuarios(db4oClient);
			//recargamos el menu principal
			TUI.appControl();
			break;

		case 6:
			PostgresDAO.selectUsuarioConMasDesnivel();
			//recargamos el menu principal
			TUI.appControl();
			break;

		case 7:
			Db4oDAO.recuperaUsuarioMaxConsultas(db4oClient);
			//recargamos el menu principal
			TUI.appControl();
			break;
			
		case 8:
			//recupramos todas las resumenes
			PostgresDAO.selectRutaMasCorta();
			//recargamos el menu principal
			TUI.appControl();
			break;

		case 9:
			//recupramos todas las resumenes
			PostgresDAO.selectRutaMasLarga();
			//recargamos el menu principal
			TUI.appControl();
			break;
			
		case 10:
			BasexDAO.printRutasContent();
			//recargamos el menu principal
			TUI.appControl();
			break;
			
		case 11:
			PostgresDAO.selectIdAndNameFromRutas();
			//recargamos el menu principal
			TUI.appControl();
			break;
			
		case 12:
			seleccionaRutaHtml();
			//recargamos el menu principal
			TUI.appControl();
			break;

		default:
			break;
		}
//		//recargamos el menu principal
//		TUI.appControl();
	}
	
	
	/**
	 * Elimina por completo los contenidos de basex, db4o y postgresQl.
	 * 
	 * @return void
	 */
	public static void peticionEliminarDB(){
		System.out.println(" ATENCIÓN, la siguiente acción restaurará por completo la BD dejándola vacía.");
		System.out.println(" Eliminará y restaurará los siguientes sistemas :\n");
		System.out.println("\tdb4o       : objetos 'Usuario' y 'ConsultaRuta'");
		System.out.println("\tbaseX      : ficheros xml y raw");
		System.out.println("\tpostgresQl : base de datos gpxdb\n");
		System.out.println(" Desea continuar ?\n");
		System.out.println("\t1. Contiunuar.\n\t2. Volver atrás.");
		System.out.print("\n > ");
		String opcionSeleccionada = sc.nextLine();
		if(opcionSeleccionada.equals("1")){
			
			System.out.print("\n Introduzca contraseña de administrador : ");
			
			opcionSeleccionada = sc.nextLine();
			if(opcionSeleccionada.equals("12345")){
				BasexDAO.deleteBasexDB();
				PostgresDAO.deletePostgresGpxDB();
				Db4oDAO.deleteDb4o(db4oClient);
				
				System.out.println("\n BD restaurada.");
			}
			else{
				TUI.printError("contraseña incorrecta");
				return;
			}
		}
		else if(opcionSeleccionada.equals("2")){}
		else{
			System.out.println(" Opción desconocida.");
		}
	}
	
	
	/**
	 * Imprime por pantalla una lista de los ficheros html creados
	 * en la carpeta /home/userName/MisRutasAppHtmlFiles.
	 * Al seleccionar el número correspondiente con el fichero
	 * html, este se abrirá con firefox;
	 * 
	 * @return void
	 */
	public static void seleccionaRutaHtml(){
		String rutaMisRutasAppHtmlFiles = System.getProperty("user.home") + "/MisRutasAppHtmlFiles";
		
		File directorioMisRutasAppHtmlFiles = new File(rutaMisRutasAppHtmlFiles);
		
		File[] files = 	directorioMisRutasAppHtmlFiles.listFiles();
		
		//si no hay ficheros salimos
		if(files.length == 0){
			TUI.printError("aún no se han creados rutas en html");
			return;
		}
		
		System.out.println("\n Ficheros :\n");
		for(int i = 0;i<files.length;i++){
			System.out.println("\t" + (i+1) + " : " + files[i].getName());
		}
		System.out.println();
		
		System.out.print(" Escribe el número de un fichero para abrirlo con firefox : ");
		int numeroFichero = 0;
		
		try{
			numeroFichero = Integer.parseInt(sc.nextLine()) - 1;
		}
		catch(NumberFormatException e){
			TUI.printError("opción desconocida");
			return;
		}
		
		if( (numeroFichero < 0) || (numeroFichero > files.length-1) ){
			TUI.printError("elige un número entre 1 y " + files.length );
			return;
		}
		
		Runtime rt = Runtime.getRuntime();
		
		try {
			rt.exec("/usr/bin/firefox " + System.getProperty("user.home")+"/MisRutasAppHtmlFiles/"+files[numeroFichero].getName());
		} catch (IOException e) {
			System.out.println("Error, firefox no se encuentra en : /usr/bin/firefox");
			return;
		}
	}
	
	
	/**
	 * Almacena la acción realizada por el usuario el la BD db4o, indicando el nombre de usuario,
	 * la acción realizada, y si la acción ha tenido éxito o no. Una vez hecho esto, recarga
	 * el menú principal de la app.
	 * 
	 * @param userName el nombre de usuario, 'visitante' si no se requiere login
	 * @param actionCode código de la acción realizada :
	 * 					 0. Darse de alta
	 * 					 1. Subir una ruta
	 * 					 2. Consultar mis rutas.
	 * 					 3. Ver una ruta.
	 * 					 4. Ver todas las rutas. 
	 * @param successOnAction true si la acción tiene éxito, flase en otro caso
	 * @return void
	 */
	public static void almacenaAccionEnDb4o(String userName, int actionCode, boolean successOnAction){
		//almacenamos la acción en la db4o
		Usuario u = new Usuario(userName, actionCode, successOnAction);
		db4oClient.store(u);
		db4oClient.commit();
		//recargamos el menu principal
		System.out.println();
		TUI.appControl();
	}

	
	/**
	 * Se crea el cliente del servidor de db4o y lo mantenemos abierto
	 */
	@Override
	public void run() {
		try{
			db4oClient = 
					Db4oClientServer.openClient("127.0.0.1", 8732, "user", "1234");//podemos usar localhost o 127.0.0.1
			while(!stopDb4oClient){
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			db4oClient.close();
		}
		
	}

}
