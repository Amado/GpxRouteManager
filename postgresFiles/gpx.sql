
SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;


CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;


CREATE TABLE documentos (id integer NOT NULL, nombre text, id_ruta integer);


ALTER TABLE public.documentos OWNER TO postgres;


CREATE SEQUENCE documentos_id_seq START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;


ALTER TABLE public.documentos_id_seq OWNER TO postgres;


ALTER SEQUENCE documentos_id_seq OWNED BY documentos.id;


CREATE TABLE rutas ( id integer NOT NULL,nombre text NOT NULL,descripcion text,desnivel double precision,distancia decimal,nombrex text NOT NULL,user_id integer);


ALTER TABLE public.rutas OWNER TO postgres;


CREATE SEQUENCE rutas_id_seq START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;


ALTER TABLE public.rutas_id_seq OWNER TO postgres;


ALTER SEQUENCE rutas_id_seq OWNED BY rutas.id;


CREATE TABLE usuarios (id integer NOT NULL,nombre text NOT NULL,pass character varying(32));


ALTER TABLE public.usuarios OWNER TO postgres;


CREATE SEQUENCE usuarios_id_seq START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1;


ALTER TABLE public.usuarios_id_seq OWNER TO postgres;


ALTER SEQUENCE usuarios_id_seq OWNED BY usuarios.id;


ALTER TABLE ONLY documentos ALTER COLUMN id SET DEFAULT nextval('documentos_id_seq'::regclass);


ALTER TABLE ONLY rutas ALTER COLUMN id SET DEFAULT nextval('rutas_id_seq'::regclass);


ALTER TABLE ONLY usuarios ALTER COLUMN id SET DEFAULT nextval('usuarios_id_seq'::regclass);



SELECT pg_catalog.setval('documentos_id_seq', 1, false);

SELECT pg_catalog.setval('rutas_id_seq', 1, false);

SELECT pg_catalog.setval('usuarios_id_seq', 1, false);


ALTER TABLE ONLY documentos ADD CONSTRAINT documentos_pkey PRIMARY KEY (id);


ALTER TABLE ONLY rutas ADD CONSTRAINT rutas_pkey PRIMARY KEY (id);


ALTER TABLE ONLY usuarios ADD CONSTRAINT usuarios_pkey PRIMARY KEY (id);


ALTER TABLE ONLY documentos ADD CONSTRAINT documentos_id_ruta_fkey FOREIGN KEY (id_ruta) REFERENCES rutas(id);


ALTER TABLE ONLY rutas ADD CONSTRAINT rutas_user_id_fkey FOREIGN KEY (user_id) REFERENCES usuarios(id);


REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


