GpxRouteManager is a TUI (Text User Interface) application for the administration of GPX routes, 
and is an approach about how to use under one platform, Java persistence technologies as 
BaseX, db4o, and PostgreSQL server connection.

Technologies :

	BaseX
	DOM
	XSL
	db4o
	JDBC
	postgresQL
	Threads

basex: Basex used as document server, will store the xml with routes, videos and photos uploaded by users.

PostgreSQL will store users, routes and filenames. You can find the db already mounted in the docs folder.

Db4o: will record the users activity.

You will find all the project documentation in docs folder.

For the app operation is very important that:

1. You must install in your computer the Postgresql sql db engine. Once installed create a DB named gpxdb and 
import the db file placed in docs folder. Please visit PostgresQl web to installation instructions : http://www.postgresql.org/

2. gpxdb must have a user named 'test' with permissions to the DB 'gpxdb'

3. postgresql service must be initialized before starting the app.

To initialize the service, as root: systemctl start posgresql.service
Better if the service starts directly with the computer, as root: systemctl enable postgresql.service

4. Do not modify in any way the following directories, or the app will not work:

	project folder/basexFiles
	project folder/db4oFiles
	project folder/postgresFiles
	project folder/xsl
	project folder/resumenesRutas
	project folder/resumenesRutas
